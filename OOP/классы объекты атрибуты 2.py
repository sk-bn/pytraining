"""
Подвиг 5. Объявите пустой класс с именем Car. С помощью функции setattr() добавьте в этот класс атрибуты:

model: "Тойота"
color: "Розовый"
number: "П111УУ77"
Выведите на экран значение атрибута color, используя словарь __dict__ класса Car.
"""
"""
Шпаргалка:

setattr(ClassName, "attribute_name", "value_of_attribute")

ClassName.__dict__["attribute_name"]
"""

class Car:
    pass


setattr(Car, 'model', "Тойота")
setattr(Car, 'color', "Розовый")
setattr(Car, 'number', "П111УУ77")

print(Car.__dict__['color'])

"""
class Car:
    pass

d = {'model': "Тойота", 'color': "Розовый", 'number': "О111АА77"}

for n in d:
   setattr(Car, n, d[n])

print(Car.__dict__['color'])
"""

"""
class Car:
    pass
d = {
    'model': "Тойота",
    'color': "Розовый",
    'number': "О111АА77"
}

[setattr(Car,k,v) for k,v in d.items()]

print(Car.__dict__['color'])

"""