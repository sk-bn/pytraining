"""
Подвиг 9. Объявите класс с именем Figure и двумя атрибутами:

type_fig: 'ellipse'
color: 'red'
Создайте экземпляр с именем fig1 этого класса и добавьте в него следующие локальные атрибуты:

start_pt: (10, 5)
end_pt: (100, 20)
color: 'blue'
Удалите из экземпляра класса свойство color и выведите на экран список всех локальных свойств (без значений)
объекта fig1 в одну строчку через пробел в порядке, указанном в задании.
"""


class Figure:
    type_fig = 'ellipse'
    color = 'red'


fig1 = Figure()

d = {
    'start_pt': (10, 5),
    'end_pt': (100, 20),
    'color': 'blue'
}

[setattr(fig1, k, v) for k, v in d.items()]

delattr(fig1, 'color')

# print(*[i for i in fig1.__dict__.keys()])
print(*fig1.__dict__)

