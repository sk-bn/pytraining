"""
Подвиг 5. Определите функцию с именем get_list_dig,
которая возвращает список только из числовых значений переданной ей коллекции (список или кортеж).

Сигнатура функции должна быть, следующей:

def get_list_dig(lst): ...

Вызывать функцию не нужно, только определить. Также ничего не нужно выводить на экран.

P. S. Не забудьте про необходимость различения булевых значений (False, True) от целочисленных.
"""
def get_list_dig(lst):
    return list(filter(lambda x: type(x) in (float, int), lst))

"""
def get_list_dig(lst):
    return [x for x in lst if type(x) in (int, float, complex)]
    
# также существуют еще fraction и decimal
"""

"""
from typing import Union, List


def get_list_dig(collection: Union[tuple, list]) -> List[Union[int, float, complex]]:
    # Принимает список или кортеж и возвращает список чисел полученной коллекции.
    return list(filter(lambda x: type(x) in (int, float, complex), collection))

"""