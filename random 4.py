"""
Подвиг 5. Вводится таблица целых чисел, записанных через пробел.
Необходимо перемешать столбцы этой таблицы, используя функции
shuffle и zip и вывести результат на экран (также в виде таблицы).

P. S. Для считывания списка целиком в программе уже записаны начальные строчки.

Sample Input:

1 2 3 4
5 6 7 8
9 8 6 7
Sample Output:

4 1 3 2
8 5 7 6
7 9 6 8
"""
import sys
import random

# установка "зерна" датчика случайных чисел, чтобы получались одни и те же случайные величины
random.seed(1)

# считывание списка из входного потока
# lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу
lst_in = ['1 2 3 4', '5 6 7 8', '9 8 6 7']
# shuffle - перемешивание элементов последовательности

lst = [list(map(int, i.split())) for i in lst_in]
# [[1, 2, 3, 4], [5, 6, 7, 8], [9, 8, 6, 7]]

lst_zip = list(zip(*lst))
# [(1, 5, 9), (2, 6, 8), (3, 7, 6), (4, 8, 7)]

random.shuffle(lst_zip)
# [(4, 8, 7), (1, 5, 9), (3, 7, 6), (2, 6, 8)]

lst_out = list(zip(*lst_zip))

for i in lst_out:
    print(*i)

"""
lst = list(zip(*map(str.split, lst_in)))
random.shuffle(lst)
[print(*i) for i in zip(*lst)]
"""

"""
lst_in = list(map(str.strip, sys.stdin.readlines()))
lst_in = [list(map(int, i.split())) for i in lst_in]
lst_in = list(zip(*lst_in))
random.shuffle(lst_in)
[print(*i) for i in zip(*lst_in)]
"""

"""
import sys
import random as rnd

columns = list(zip(*map(str.split, sys.stdin)))
rnd.seed(1)
rnd.shuffle(columns)
for row in zip(*columns):
    print(*row)
"""