import random

# import numpy as np  # импортируем в задачу модуль

# установка "зерна" датчика случайных чисел, чтобы получались одни и те же случайные величины
random.seed(1)
# начальная инициализация поля (переменные P и N не менять, единицы записывать в список P)
# N = int(input())
N = 7
P = [[0] * N for i in range(N)]

# здесь продолжайте программу
M = 10


# функция проверяет что соседние клетки не содержат 1
def is_isolate(i, j):
    return sum([P[i][j] + P[i - 1][j - 1], P[i - 1][j], P[i - 1][j + 1],
                P[i][j - 1], P[i][j + 1], P[i + 1][j - 1],
                P[i + 1][j], P[i + 1][j + 1]]) == 0


# строим границу из нулей вокруг матрицы P
# P = np.pad(P, pad_width=1, mode='constant', constant_values=0)
def surround_with_zeros(lst):
    n = len(lst)
    lst.insert(0, [0] * n)
    lst.append([0] * n)
    for i in lst:
        i.insert(0, 0)
        i.append(0)


def remove_surrounding_zeros(lst):
    n = len(lst)
    lst.pop(0)
    lst.pop()
    for i in lst:
        i.pop(0)
        i.pop()


surround_with_zeros(P)
# Заполняем поле 10-ю минами

for _ in range(M):
    while 1:
        row = random.randint(1, N)
        col = random.randint(1, N)
        if P[row][col] != 1 and is_isolate(row, col):
            P[row][col] = 1
            break


remove_surrounding_zeros(P)

print(*P, sep='\n')

"""
import random

random.seed(1)

N = int(input())
P = [[0] * N for i in range(N)]

for _ in range(10):
    while True:
        r, c = random.randint(0, N - 1), random.randint(0, N - 1)
        if not sum(P[i][y] for i in range(max(0, r - 1), min(r + 2, N)) 
                           for y in range(max(0, c - 1), min(c + 2, N))):
            P[r][c] = 1
            break  
"""

"""

Павел Кузьмин
в прошлом году
проверяем только существующих соседей

Верное решение #539827814
Python 3
import random
# установка "зерна" датчика случайных чисел, чтобы получались одни и те же случайные величины
random.seed(1)
# начальная инициализация поля (переменные P и N не менять, единицы записывать в список P)
N = int(input())
P = [[0] * N for i in range(N)]

# здесь продолжайте программу
count = 0
while count < 10:
    j, i = (random.randint(0, N - 1) for _ in "ji")
    if not P[j][i]:
        in_touch = [P[y][x]
                    for y in range(j - 1, j + 2) if y in range(N)
                    for x in range(i - 1, i + 2) if x in range(N)]
        if not any(in_touch):
            P[j][i] = 1
            count += 1
"""

"""
import random
# установка "зерна" датчика случайных чисел, чтобы получались одни и те же случайные величины
random.seed(1)
# начальная инициализация поля (переменные P и N не менять, единицы записывать в список P)
N = int(input())
P = [[0] * N for i in range(N)]

# здесь продолжайте программу
k = [(r, c) for r in range(0, N, 2) for c in range(0, N, 2)]
for r, c in random.sample(k, min(10, len(k))):
    P[r][c] = 1
"""

"""
import random as rnd

rnd.seed(1)
N = int(input())
P, M, deltas = [[0] * N for i in range(N)], 10, (-1, 0, 1)

while M:
    r, c = map(rnd.randrange, (N,) * 2)
    if all(not (0 <= r + dr < N and 0 <= c + dc < N) 
           or P[r + dr][c + dc] == 0 for dr in deltas for dc in deltas):
        P[r][c], M = 1, M - 1
"""

"""
Вариант №2. Проверка выполняется только для соседних клеток, т.е. всего проверяется 9 пар координат (включая сгенерированную). 
Единичные приращения координат по 8-и направлениям, dirs, генерируются заранее для эффектвности и сокращения кода. 
Это делается с помощью функции product из модуля itertools вместо двух вложенных циклов. 
Так же перенёс проверку выхода координат за границы матрицы в раздел if генератора списочного выражения для повышения 
читабельности кода. Сочетание функции all с генератором списочного выражения обеспечивает "ленивое" выполнение 
проверки участка игрового поля, проверка заканчивается при первом обнаружении ненулевой клетки.

Верное решение #532500099
Python 3
import random as rnd, itertools as its

rnd.seed(1)
N = int(input())
P, M, dirs = [[0] * N for i in range(N)], 10, tuple(its.product((-1, 0, 1), repeat=2))

while M:
    r, c = map(rnd.randrange, (N,) * 2)
    if all(P[r + dr][c + dc] == 0 for dr, dc in dirs if 0 <= r + dr < N and 0 <= c + dc < N):
        P[r][c], M = 1, M - 1
"""