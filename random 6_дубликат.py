import random

# import numpy as np  # импортируем в задачу модуль

# установка "зерна" датчика случайных чисел, чтобы получались одни и те же случайные величины
random.seed(1)
# начальная инициализация поля (переменные P и N не менять, единицы записывать в список P)
# N = int(input())
N = 7
P = [[0] * N for i in range(N)]

# здесь продолжайте программу
M = 10


# функция проверяет что соседние клетки не содержат 1
def is_isolate(i, j):
    return sum([P[i][j] + P[i - 1][j - 1], P[i - 1][j], P[i - 1][j + 1],
                P[i][j - 1], P[i][j + 1], P[i + 1][j - 1],
                P[i + 1][j], P[i + 1][j + 1]]) == 0


# строим границу из нулей вокруг матрицы P
# P = np.pad(P, pad_width=1, mode='constant', constant_values=0)
def surround_with_zeros(lst):
    n = len(lst)
    lst.insert(0, [0] * n)
    lst.append([0] * n)
    for i in lst:
        i.insert(0, 0)
        i.append(0)


def remove_surrounding_zeros(lst):
    n = len(lst)
    lst.pop(0)
    lst.pop()
    for i in lst:
        i.pop(0)
        i.pop()


surround_with_zeros(P)
# Заполняем поле 10-ю минами

for _ in range(M):
    while 1:
        row = random.randint(1, N)
        col = random.randint(1, N)
        if P[row][col] != 1 and is_isolate(row, col):
            P[row][col] = 1
            break


remove_surrounding_zeros(P)

print(*P, sep='\n')
