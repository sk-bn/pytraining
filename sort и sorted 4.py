"""
Подвиг 5. На вход программы поступают два списка целых чисел (каждый в отдельной строке),
записанных в одну строчку через пробел.
Длины списков могут быть разными.
Необходимо первый список отсортировать по возрастанию, а второй - по убыванию.
Полученные пары из обоих списков сложить друг с другом и получить новый список чисел.
Результат вывести на экран в виде строки чисел через пробел.

P. S. Подсказка: не забываем про функцию zip.

Sample Input:

7 6 4 2 6 7 9 10 4
-4 5 10 4 5 65
Sample Output:

67 14 9 11 10 3
"""
lst_in1 = list(map(int, input().split()))
lst_in2 = list(map(int, input().split()))

lst_in1.sort()
l2 = sorted(lst_in2, reverse=True)

lst = list(zip(lst_in1, l2))
lst_out = list(map((lambda x: x[0] + x[1]), lst))

print(*lst_out)

"""
lst_1 = sorted(map(int, input().split()))
lst_2 = sorted(map(int, input().split()))[::-1]

print(*map(sum, zip(lst_1, lst_2)))
"""

"""
lst1 = sorted(map(int, input().split()))
lst2 = sorted(map(int, input().split()), reverse=True)

lst = map(lambda x, y: x + y, lst1, lst2)
print(*lst)
"""