"""
Подвиг 6. На вход программы поступает список товаров в формате:

название_1:цена_1
...
название_N:цена_N

Необходимо преобразовать этот список в словарь,
ключами которого выступают цены (целые числа),
а значениями - соответствующие названия товаров.
Необходимо написать функцию, которая бы принимала на входе словарь
и возвращала список из наименований трех наиболее дешевых товаров.

Вызовите эту функцию и отобразите на экране полученный список
в порядке возрастания цены в одну строчку через пробел.

P. S. Для считывания списка целиком в программе уже записаны начальные строчки.

Sample Input:

смартфон:120000
яблоко:2
сумка:560
брюки:2500
линейка:10
бумага:500
Sample Output:

яблоко линейка бумага
"""
import sys

# считывание списка из входного потока
# lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список строк lst_in)
lst_in = ['смартфон:120000', 'яблоко:2', 'сумка:560', 'брюки:2500', 'линейка:10', 'бумага:500']
lst_in = tuple([tuple(i.split(':')) for i in lst_in])
d = dict((int(y), x) for x, y in lst_in)


# {'120000': 'смартфон', '2': 'яблоко', '560': 'сумка', '2500': 'брюки', '10': 'линейка', '500': 'бумага'}

def func(d):
    d_sort = sorted(d)
    # [2, 10, 500, 560, 2500, 120000]
    d_out = [d[i] for i in d_sort]
    return d_out[:3]


print(*func(d))

"""
import sys
lst_in = list(map(str.strip, sys.stdin.readlines()))

def get_cheap(d): 
    return list(dict(sorted(d.items())[:3]).values())


dct = {int(b) : a for a, b in [x.split(':') for x in lst_in]}
print(*get_cheap(dct))
"""

"""
import sys

def best3(d):
    return [d[key] for key in sorted(d)[:3]]

lst_in = list(map(str.strip, sys.stdin.readlines()))
d = {}
for s in lst_in:
    item, price = s.split(':')
    d[int(price)] = item
    
print(*best3(d))
"""

"""
import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список строк lst_in)


def get_cheap(lst):
    d = dict(map(lambda x: (int(x[1]), x[0]), map(lambda y: y.split(':'), lst)))
    
    for k, v in sorted(d.items()):
        yield v
        
        
x = get_cheap(lst_in)

for _ in range(3):
    print(next(x), end=' ')
"""