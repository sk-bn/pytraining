"""
Значимый подвиг 4. Имеется таблица с данными, представленная в формате:

Номер;Имя;Оценка;Зачет
1;Иванов;3;Да
2;Петров;2;Нет
...
N;Балакирев;4;Да

Эти данные необходимо представить в виде двумерного (вложенного) кортежа.
Все числа должны быть представлены как целые числа.
Затем, отсортировать данные так, чтобы столбцы шли в порядке:

Имя;Зачет;Оценка;Номер

Реализовать эту операцию с помощью сортировки.
Результат должен быть представлен также в виде двумерного кортежа
и присвоен переменной с именем t_sorted.

Программа ничего не должна выводить на экран,
только формировать двумерный кортеж с переменной t_sorted.

P. S. Для считывания списка целиком в программе уже записаны начальные строчки.

Sample Input:

Номер;Имя;Оценка;Зачет
1;Портос;5;Да
2;Арамис;3;Да
3;Атос;4;Да
4;д'Артаньян;2;Нет
5;Балакирев;1;Нет
Sample Output:

True
"""

import sys

# считывание списка из входного потока (не меняйте переменную lst_in в программе)
# lst_in = list(map(str.strip, sys.stdin.readlines()))
lst_in = ['Номер;Имя;Оценка;Зачет', '1;Портос;5;Да', '2;Арамис;3;Да', '3;Атос;4;Да', "4;д'Артаньян;2;Нет",
          '5;Балакирев;1;Нет']

# # здесь продолжайте программу (используйте список строк lst_in)
lst = tuple([tuple(i.split(';')) for i in lst_in])
# (('Номер', 'Имя', 'Оценка', 'Зачет'), ('1', 'Портос', '5', 'Да'), ('2', 'Арамис', '3', 'Да'),
# ('3', 'Атос', '4', 'Да'), ('4', "д'Артаньян", '2', 'Нет'), ('5', 'Балакирев', '1', 'Нет'))

lst_int = tuple([[int(j) if j.isdigit() == True else j for j in i] for i in lst])
# (['Номер', 'Имя', 'Оценка', 'Зачет'], [1, 'Портос', 5, 'Да'], [2, 'Арамис', 3, 'Да'], 
# [3, 'Атос', 4, 'Да'], [4, "д'Артаньян", 2, 'Нет'], [5, 'Балакирев', 1, 'Нет'])

# Имя;Зачет;Оценка;Номер
n = [1, 3, 2, 0]
t_sorted = tuple([tuple(sorted(i, key=lambda x: n.index(i.index(x)))) for i in lst_int])

print(t_sorted)
"""
# подсказка
n=[0,2,1]
a=("a","b","c")
print(sorted(a, key=lambda x: n.index(a.index(x))))
"""

"""
import sys

# считывание списка из входного потока (не меняйте переменную lst_in в программе)
lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список строк lst_in)
order = "Имя;Зачет;Оценка;Номер"
t = tuple(tuple(int(st) if st.isdigit() else st for st in row.split(";")) for row in lst_in)
t_sorted = tuple(zip(*sorted(list(zip(*t)), key=lambda x: order.find(x[0]))))
"""

"""
import sys

lst_in = list(map(str.strip, sys.stdin.readlines()))
haders = tuple('Имя;Зачет;Оценка;Номер'.split(';'))
t = tuple(tuple(int(e) if e.isdigit() else e for e in s.split(';')) for s in lst_in)
t_sorted = tuple(zip(*sorted(zip(*t), key=lambda x: haders.index(x[0]))))
"""

"""
t_sorted = tuple([
    (name, passed, int(mark) if mark.isdigit() else mark, int(num) if num.isdigit() else num) 
    for num, name, mark, passed in [el.split(';') for el in lst_in]
])
"""

"""
import sys

# считывание списка из входного потока (не меняйте переменную lst_in в программе)
lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список строк lst_in)

order = {idx: 'Имя;Зачет;Оценка;Номер'.split(';').index(name) 
         for idx, name in enumerate('Номер;Имя;Оценка;Зачет'.split(';'))}

t_sorted = tuple(map(tuple, (sorted((int(i) if i.isdigit() else i for i in el.split(';')), 
                                    key=lambda x: order[el.split(';').index(str(x))]) for el in lst_in)))
"""

"""
import sys

# считывание списка из входного потока (не меняйте переменную lst_in в программе)
lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список строк lst_in)
lst1 = list((b, d, int(c), int(a)) for a, b, c, d in (line.split(';') for line in lst_in[1:]))
a, b, c, d = lst_in[0].split(';')
lst1.insert(0, (b, d, c, a))

t_sorted = tuple(lst1)
"""
