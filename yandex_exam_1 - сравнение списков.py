"""
Есть строка А длинны N
Есть строка B длинны N

Нужно сравнить каждуюбукву.
Если буквы с идентичным интексом одинаковы, то  тип совпадение - Р (плагиат)
Если буква в строке В есть в строке А под другим индексом, то тип совпадения - S (сомнительный)
Если нет совпадений, то тип - I (невинный)
"""

a = list(input())
# a = ['A', 'B', 'C', 'B', 'C', 'Y', 'A']
b = list(input())
# b = ['Z', 'B', 'B', 'A', 'C', 'A', 'A']

# правильный ответ - IPSSPIP


ans = list(range(len(a)))
for i in range(len(a)):
    if a[i] == b[i]:
        ans[i] = 'P'
        a[i] = 0
        b[i] = 0
for i in range(len(a)):
    if b[i] == 0:
        continue
    if b[i] in a:
        if a.count(b[i]) >= b[:i + 1].count(b[i]):
            ans[i] = 'S'
        else:
            ans[i] = 'I'
    else:
        ans[i] = 'I'

print(*ans, sep='')
