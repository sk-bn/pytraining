s, m = 0, 1
c, q, r, p = 0, 1, 2, 3

n = int(input())  # число специальных дисциплин финального этапа.

# создаём двумерный массив [(s) название дисциплины, (m) max кол-во участников]
lst_in_first = [input().split(',') for i in range(n)]
for i in lst_in_first:
    i[1] = int(i[1])
# [['ear_flying', 1], ['sun_bathing', 1]]

k = int(input())  # число участников отборочного этапа соревнования.

'''
создаём двумерный массив 
[c - имя участника
q - название дисциплины
r - количество приемов
P - штраф]
'''
lst_in_final = [input().split(',') for i in range(k)]
for i in lst_in_final:
    i[2] = int(i[2])
    i[3] = int(i[3])
# [['cheburashka', 'ear_flying', 11, 100], ['dambo', 'ear_flying', 10, 0], ['crocodile_gena', 'sun_bathing', 11, 10]]

# транспонируем матрицу для сравнения показателей, пока не понадобилось
results = [[m[i] for m in lst_in_final] for i in range(int(len(lst_in_final[0])))]
# [['cheburashka', 'dambo', 'crocodile_gena'], ['ear_flying', 'ear_flying', 'sun_bathing'], [11, 10, 11], [100, 0, 10]]

winner = []

for i in lst_in_first:
    m = []
    for j in lst_in_final:
        if i[s] == j[q]:
            m.append(j)
    # print(m)
    # [['cheburashka', 'ear_flying', 11, 100], ['dambo', 'ear_flying', 10, 0]]
    # [['crocodile_gena', 'sun_bathing', 11, 10]]
    if len(m) == 1:
        winner.append(m[0][c])
    for j in range(len(m) - 1):
        if m[j + 1][r] > m[j][r]:
            winner.append(m[j + 1][c])
        elif m[j + 1][r] == m[j][r]:
            if m[j + 1][p] > m[j][p]:
                winner.append(m[j + 1][c])
            else:
                winner.append(m[j+1][c])
        else:
            winner.append(m[j][c])
print(winner)

# надо отсортировать массивы по возрастанию показателей упражнений
# + добавить проверку на количество участников