#!/usr/bin/env python3

w,l,r=map(int,input().split())
L=list(map(int,input().strip().split()))
R=list(map(int,input().strip().split()))

Lsum = 0
for i in range(0, len(L)):
    Lsum += L[i]

Rsum = 0
for i in range(0, len(R)):
    Rsum += R[i]

H=1;
while True:
    Wtmp=(Lsum+Rsum)/H
    if Wtmp < w:
        break;
    H=H+1

print(H)
