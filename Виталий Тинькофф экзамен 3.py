#!/usr/bin/env python3

t,k=map(int,input().split())

S=["-"]*t
T=[0]*t
C=[0]*t
for i in range(0, t):
    a = input().split()
    S[i] = a[0]
    T[i] = int(a[1])
    C[i] = int(a[2])

Q=list(map(int,input().strip().split()))

for i in range(0, k):
    tsum = 0
    wait = 0
    time = 0
    q=Q[i]
    for j in range(0, t):
        if j > 0:
            tsum+=(T[j]-T[j-1])*wait

        time = T[j]
        if S[j] == '+':
            q+=C[j]
            if wait > 0:
                if q>=wait:
                    q-=wait
                    wait = 0
                else: # q<C[j]
                    wait -= q
                    q = 0
        else: # S[j]=='-'
            if q>=C[j]:
                q-=C[j]
            else: # q<C[j]
                wait += C[j] - q
                q = 0

    if wait>0:
        tsum="INFINITY"
    print(tsum)
