#!/usr/bin/env python3

def dijkstra(graph, src):
    visited = []
    distance = {src: 0}
    node = list(range(len(graph[0])))
    if src in node:
        node.remove(src)
        visited.append(src)
    else:
        return None
    for i in node:
        distance[i] = graph[src][i]
    prefer = src
    while node:
        _distance = float('inf')
        for i in visited:
            for j in node:
                if graph[i][j] > -1:
                    if _distance > distance[i] + graph[i][j]:
                        _distance = distance[j] = distance[i] + graph[i][j]
                        prefer = j
        visited.append(prefer)
        node.remove(prefer)
    return distance


a,b,x,y=map(int,input().split())

m = min(a,b,x,y)
M = max(a,b,x,y)
size = M

MM = [[-1] * size for i in range(size)]

for i in range(0, size):
    for j in range(0, size):
        if i==j+1 or i==j-1:
            MM[i][j]=1

MM[x-1][y-1] = MM[y-1][x-1] = 0;

distance = dijkstra(MM, a-1)
print(distance[b-1])
