#!/usr/bin/env python3

s,d=map(int,input().split())
e=[0]*s
for i in range(0, s):
    e[i] = int(input())

maxs = [0]*d
e1 = e.copy()
ii = [0]*d
for i in range(0, d):
    maxs[i] = max(e1);
    ii[i] = e1.index(maxs[i])
    e1.pop(ii[i])

ii.sort()

sum = 0;
for i in range(0, len(ii)):
    if i<len(ii)-1:
        sector = 1
        for j in range(ii[i]+1, ii[i+1]):
            sum += e[j]*sector
            sector+=1
    else:
        sector=1
        for j in range(ii[i]+1, len(e)):
            sum += e[j]*sector
            sector+=1
        for j in range(0, ii[0]):
            sum += e[j]*sector
            sector+=1

print(sum)
