# https://stepik.org/lesson/567070/step/1?unit=561344

Код урока:

try:
    with open("out.txt", "w") as file:
        file.write("Hello")
except:
    print("Ошибка при работе с файлом")
***

try:
    with open("out.txt", "a") as file:
        file.write("Hello1\n")
        file.write("Hello2\n")
        file.write("Hello3\n")
except:
    print("Ошибка при работе с файлом")
***

try:
    with open("out.txt", "a+") as file:
        file.seek(0)
        file.write("hello4")
        file.writelines(["Hello1\n", "Hello2\n"])
        s = file.readlines()
        print(s)
except:
    print("Ошибка при работе с файлом")
***

import pickle

books = [ .......... ]

file = open("out.bin", "wb")
pickle.dump(books, file)
file.close()
***

import pickle

file = open("out.bin", "rb")
bs = pickle.load(file)
file.close()

print(bs)
***

import pickle

books1 = [ .......... ]
books2 = [ .......... ]
books3 = [ .......... ]
books4 = [ .......... ]

try:
    with open("out.bin", "wb") as file:
        pickle.dump(book1, file)
        pickle.dump(book2, file)
        pickle.dump(book3, file)
        pickle.dump(book4, file)
except:
    print("Ошибка при работе с файлом")
***

import pickle

try:
    with open("out.bin", "rb") as file:
        b1 = pickle.load(file)
        b2 = pickle.load(file)
        b3 = pickle.load(file)
        b4 = pickle.load(file)
except:
    print("Ошибка при работе с файлом")

print(b1, b2, b3, b4, sep="\n")