"""
Конечно, я могу помочь написать бота в Telegram для загрузки фотографий в указанный каталог в настройках бота. Для этого вам потребуется создать нового бота в Telegram и получить API-токен.

Затем вы можете использовать Python и библиотеку python-telegram-bot для написания бота. Вот пример кода, который загружает фотографии в указанный каталог:





В этом примере бот реагирует на команду /start, после чего пользователь может отправить фотографию. Когда фотография приходит, она сохраняется в указанный каталог. Кроме того, бот отправляет пользователю сообщение о том, что фотография успешно загружена.
"""


import telegram
import os

# Устанавливаем токен для бота
TOKEN = 'ваш API-токен'

# Устанавливаем каталог для загрузки фотографий
PHOTO_DIR = 'каталог для загрузки фотографий'

# Создаем экземпляр бота
bot = telegram.Bot(token=TOKEN)

# Обработчик команды /start
def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Привет! Отправь мне фотографию.")

# Обработчик фотографий
def handle_photo(update, context):
    # Получаем объект фотографии из обновления
    photo = update.message.photo[-1]

    # Генерируем уникальное имя файла на основе ID фотографии
    file_name = f"{photo.file_id}.jpg"

    # Загружаем фотографию в указанный каталог
    photo_file = bot.get_file(photo.file_id)
    photo_file.download(os.path.join(PHOTO_DIR, file_name))

    # Отправляем пользователю сообщение о том, что фотография успешно загружена
    update.message.reply_text("Фотография успешно загружена!")

# Создаем объекты обработчиков
from telegram.ext import CommandHandler, MessageHandler, Filters
start_handler = CommandHandler('start', start)
photo_handler = MessageHandler(Filters.photo, handle_photo)

# Регистрируем обработчики в диспетчере
from telegram.ext import Dispatcher
dispatcher = Dispatcher(bot, None)
dispatcher.add_handler(start_handler)
dispatcher.add_handler(photo_handler)

# Запускаем бота
bot.polling()
