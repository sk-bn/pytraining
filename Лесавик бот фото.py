"""
Для написания бота в телеграмм для загрузки фотографий в указанный каталог в настройках бота, вам необходимо выполнить следующие шаги:

    Создайте бота в Telegram и получите его токен. Это можно сделать через официального бота @BotFather.

    Установите необходимые библиотеки для работы с Telegram API и для загрузки файлов. Для этого можно воспользоваться командой в терминале:
    pip install python-telegram-bot requests


    Запустите бота командой python ваш_файл_с_кодом.py. После запуска бот будет ожидать сообщений с фотографиями и сохранять их в указанный каталог.

Обратите внимание, что в приведенном выше примере кода необходимо заменить значение переменных TOKEN и PHOTO_DIR на свои значения.

"""


import os
import logging
import telegram


from telegram.ext import Updater, CommandHandler, MessageHandler, Filters


# Настройки бота
TOKEN = 'ваш токен'
PHOTO_DIR = 'путь к каталогу для сохранения фотографий'

# Настройки логгирования
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


# Функция для загрузки фотографий в указанный каталог
def save_photo(update, context):
    # Получаем объект фотографии из сообщения
    photo_file = update.message.photo[-1].get_file()
    # Генерируем имя файла из идентификатора чата и номера сообщения
    file_name = f'{update.message.chat_id}_{update.message.message_id}.jpg'
    # Сохраняем файл на диск
    photo_file.download(f'{PHOTO_DIR}/{file_name}')
    # Отправляем пользователю сообщение о том, что файл успешно сохранен
    update.message.reply_text(f'Фотография сохранена: {file_name}')


def main():
    # Создаем экземпляр бота и получаем диспетчер событий
    updater = Updater(TOKEN, use_context=True)
    dispatcher = updater.dispatcher

    # Создаем обработчик команды start
    start_handler = CommandHandler('start', lambda update, context: update.message.reply_text('Бот для загрузки фотографий в указанный каталог'))
    dispatcher.add_handler(start_handler)

    # Создаем обработчик сообщений с фотографиями
    photo_handler = MessageHandler(Filters.photo, save_photo)
    dispatcher.add_handler(photo_handler)

    # Запускаем бота
    updater.start_polling()
    logger.info('Бот запущен')
    updater.idle()


if __name__ == '__main__':
    main()
