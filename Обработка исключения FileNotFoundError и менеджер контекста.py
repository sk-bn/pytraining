# https://stepik.org/lesson/567069/step/1?unit=561343
"""
Конспект темы:

1.Пример использования
try / except / else / finally
"""

f = open('1.txt')

ints = []

try:  # блок критического кода

    for line in f:
        ints.append(int(line))

except ValueError:  # если произошла ошибка в try и имя ошибки = ValueError

    print('Это не число. Выходим.')

except Exception:  # если произошла ошибка в try и имя ошибки = Exception

    print('Это что ещё такое?')

else:  # выполнится если в try не будет ошибок

    print('Всё хорошо.')

finally:  # выполнится в любом случае

    f.close()

    print('Я закрыл файл.')

"""
2. Некоторые имена ошибок:

   FileNotFoundError - файл не найден (неверный путь к файлу или не существует файла)

   Exception - все остальные ошибки (можно оставить пустым, тогда обрабатываются и

                          системные исключения, которые лучше не трогать)

   ZeroDivisonError - деление на ноль

   ValueError - недопустимое значение, например int("Hello") или int(8.2)

   TypeError - несовпадение типов, например 'a' + 20
"""

# 3. Код урока:

try:

    file = open("my_file.txt", encoding="utf-8")

    try:

        s = file.readlines()

        int(s)

        print(s)

    finally:

        file.close()

except FileNotFoundError:

    print("Невозможно открыть файл")

except:

    print("Ошибка при работе с файлом")
** *

try:

    with open("my_file.txt", encoding="utf-8") as file:

        s = file.readlines()

        int(s)

        print(s)

except FileNotFoundError:

    print("Невозможно открыть файл")

except:

    print("Ошибка при работе с файлом")

finally:

    print(file.closed)


"""
вместо

except
лучше использовать такую конструкцию:

except Exception
и да по коду видно что int(s) вызывает ошибку и новое, что я узнал, 
что она как и в случае со множественным наследованием классов, 
использует исключение из уровня выше, замечательно не правда ли? 
То есть вложенная ошибка отработалась в родительском try except. 
Не надо громоздить на каждое вложение своё исключение. 
"""

try:
    file = open("my_file.txt", encoding="utf-8")
    try:
        s = file.readlines()
        int(s)
        print(s)
    finally:
        file.close()
except Exception as exception:
    print(exception.__doc__, exception,
          "Ошибка при работе с файлом", sep='\n')

"""
А ещё лучше, согласно PEP, главному рукодству по написанию правильного кода всегда 
лучше чётко указывать какие ошибки отлавливаются (это скорее всего будет рассмотрено в подробной теме). 

except (FileNotFoundError, ValueError, IndexError)
Таким образом будёт понятно, что является прогнозируемой ошибкой, 
а что неожиданной. На прогнозируемый мы уже написали заглушку, 
а неожиданная покажется обычным красным цветом с остановкой кода.

И конечно явно видно преимущество конструкции 
with ... as file vs. open(file) .. close(file), потому как не нужно искать концы.
"""

try:
    with open("my_fil.txt", encoding="utf-8") as file:
        s = file.readlines()
        int(s)
        print(s)
except Exception as exception:
    print(exception.__doc__, exception,
          "Ошибка при работе с файлом", sep='\n')
