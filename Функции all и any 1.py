"""
Подвиг 1. Вводится строка целых чисел через пробел.
Необходимо определить, являются ли все эти числа четными.
Вывести True, если это так и False - в противном случае.

Задачу реализовать с использованием одной из функций: any или all.

Sample Input:

2 4 6 8 22 56
Sample Output:

True

"""
lst = list(map(int, input().split()))
print(all(map(lambda x: x % 2 == 0, lst)))

"""
print(not any(int(i) % 2 for i in input().split()))

"""

"""
lst = map(int, input().split())

print(all(map(lambda x: x % 2 == 0, lst)))
"""

"""
print(not any(e & 1 for e in map(int, input().split())))
"""

"""
print(all(map(lambda x: int(x) % 2 == 0, input().split())))
"""

"""
print(all(not int(c) % 2 for c in input().split()))

"""