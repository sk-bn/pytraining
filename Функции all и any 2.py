"""
Подвиг 2. Вводится строка вещественных чисел через пробел.
Необходимо определить, есть ли среди них хотя бы одно отрицательное.
Вывести True, если это так и False - в противном случае.

Задачу реализовать с использованием одной из функций: any или all.

Sample Input:

8.2 -11.0 20 3.4 -1.2
Sample Output:

True
"""

lst = list(map(float, input().split()))
print(any(map(lambda x: x < 0, lst)))

"""
print('-' in input())
"""

"""
print(any(float(c) < 0 for c in input().split()))
"""

"""
print(any(i < 0 for i in map(float, input().split())))
"""

"""
print(any(map(lambda x: float(x)<0, input().split())))
"""