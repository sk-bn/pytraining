"""
Подвиг 4. Вводятся оценки студента в одну строчку через пробел.
Необходимо определить, имеется ли в этом списке хотя бы одна оценка ниже тройки.
Если это так, то вывести на экран строку "отчислен", иначе - "учится".

Задачу реализовать с использованием одной из функций: any или all.

Sample Input:

3 3 3 2 3 3
Sample Output:

отчислен
"""
lst = list(map(int, input().split()))
verdict = any(map(lambda x: x < 3, lst))

print('отчислен') if verdict == True else print('учится')

"""
print(['учится', 'отчислен'][any(int(c) < 3 for c in input().split())])
"""

"""
lst = map(int, input().split())

print('отчислен' if any(i < 3 for i in lst) else 'учится')
"""

"""
print(('учится', 'отчислен',)[any(e in '12' for e in input().split())])
"""