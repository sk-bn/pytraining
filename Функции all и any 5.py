"""
Подвиг 5. Вводится текущее игровое поле для игры "Крестики-нолики" в виде следующей таблицы:

# x o
x # x
o o #

Здесь # - свободная клетка.
Нужно объявить функцию с именем is_free,
на вход которой поступает игровое поле в виде двумерного (вложенного) списка.
Данная функция должна возвращать True, если есть хотя бы одна свободная клетка и False - в противном случае.

Сигнатура функции должна быть, следующей:

def is_free(lst): ...

Вызывать функцию не нужно, только определить.
Также ничего не нужно выводить на экран.
Задачу реализовать с использованием одной из функций: any или all.

P. S. Считывание входного списка делать не нужно!!! Только определить функцию.

Sample Input:

# x o
x # x
o o #
Sample Output:

True
"""
lst_in = ['# x o', 'x # x', 'o o #']


def is_free(lst):
    return any(any(map(lambda x: x == "#", line)) for line in lst)


print(is_free(lst_in))

"""
def is_free(lst):
    ret = tuple(any(map(lambda x: x=="#",line)) for line in lst)
    return any(ret)
"""

"""
def is_free(lst):
    return any(map(lambda x: '#' in x, lst))
"""

"""
def is_free(lst):
    return any('#' in i for i in lst)
"""

"""
is_free = lambda x: '#' in ''.join(x)
"""

"""
def is_free(lst):
    return any(map(lambda x: '#' in x, lst))
"""

"""

"""