"""
Хеш-таблицы — это сложная структура данных,
способная хранить большие объёмы информации и эффективно извлекать определённые элементы.

В этой структуре данных используются пары ключ / значение,
где ключ — это имя желаемого элемента, а значение — это данные, хранящиеся под этим именем.

Каждый входной ключ проходит через хеш-функцию, которая преобразует его из начальной формы в целочисленное значение,
называемое хешем. Хеш-функции всегда должны генерировать один и тот же хэш из одного и того же ввода,
должны быстро вычислять и выдавать значения фиксированной длины.
Python включает встроенную hash()функцию, ускоряющую реализацию.

Затем таблица использует хеш-код для нахождения общего местоположения желаемого значения,
называемого корзиной хранения.
После этого программе нужно будет искать нужное значение только в этой подгруппе, а не во всём пуле данных.

Помимо этой общей структуры, хеш-таблицы могут сильно отличаться в зависимости от приложения.
Некоторые могут разрешать ключи из разных типов данных,
в то время как некоторые могут иметь разные настройки сегментов или разные хеш-функции.

Преимущества:

Может скрывать ключи в любой форме в целочисленные индексы.
Чрезвычайно эффективен для больших наборов данных.
Очень эффективная функция поиска.
Постоянное количество шагов для каждого поиска и постоянная эффективность добавления или удаления элементов.
Оптимизирован в Python 3.

Недостатки:

Хэши должны быть уникальными, преобразование двух ключей в один и тот же хэш вызывает ошибку коллизии.
Ошибки коллизии требуют полного пересмотра хэш-функции.
Сложно построить для новичков.

Приложения:

Используется для больших баз данных, в которых часто ведётся поиск.
Системы поиска, использующие клавиши ввода.

"""

import pprint


class Hashtable:
    def __init__(self, elements):
        self.bucket_size = len(elements)
        self.buckets = [[] for i in range(self.bucket_size)]
        self._assign_buckets(elements)

    def _assign_buckets(self, elements):
        for key, value in elements:  # calculates the hash of each key
            hashed_value = hash(key)
            index = hashed_value % self.bucket_size  # positions the element in the bucket using hash
            self.buckets[index].append((key, value))  # adds a tuple in the bucket

    def get_value(self, input_key):
        hashed_value = hash(input_key)
        index = hashed_value % self.bucket_size
        bucket = self.buckets[index]
        for key, value in bucket:
            if key == input_key:
                return (value)
        return None

    def __str__(self):
        return pprint.pformat(self.buckets)  # pformat returns a printable representation of the object


if __name__ == '__main__':
    capitals = [
        ('France', 'Paris'),
        ('United States', 'Washington D.C.'),
        ('Italy', 'Rome'),
        ('Canada', 'Ottawa')
    ]
hashtable = Hashtable(capitals)
print(hashtable)
print(f'The capital of Italy is {hashtable.get_value("Italy")}')
