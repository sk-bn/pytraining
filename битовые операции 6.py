"""
Подвиг 7. Вводится зашифрованное слово.
Шифрование кодов символов этого слова было проведено с помощью битовой операции XOR с ключом key=123.
То есть, каждый символ был преобразован по алгоритму:

x = ord(x) ^ key

Здесь ord - функция, возвращающая код символа x. Расшифруйте введенное слово и выведите его на экран.

P. S. Подсказка: для преобразования кода в символ используйте функцию chr.

Sample Input:

ѩкю[щюлцхZ
Sample Output:

Все верно!
"""
a = input()
key = 123
strg =[chr(ord(x) ^ key) for x in a]

print("".join(strg))

"""
word = input()
key = 123

text = list(map(lambda x: ord(x) ^ key, word))  # декодируем обратно
# print(text) -> [1042, 1089, 1077, 32, 1074, 1077, 1088, 1085, 1086, 33]
result = tuple(map(lambda x: chr(x), text))  # заменяем список чисел на список символов
print(''.join(result))
"""

"""
key = 123
s = input()
l = []
for i in s:
    l.append(ord(i) ^ key)
for i in l:
    print(chr(i), end='')
"""

"""
for i in input():
    print(chr(ord(i) ^ 123), end = '')
"""

"""
[print(chr(ord(i) ^ 123), end='') for i in input()]
"""

"""
print(*map(lambda x: chr(ord(x) ^ 123), input()), sep='')
"""

"""
key=123
print(''.join(chr(ord(x) ^ key) for x in input()))
"""