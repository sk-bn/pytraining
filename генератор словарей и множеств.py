"""
Подвиг 5. Вводится текст в одну строчку со словами через пробел.
Используя генераторы множеств и словарей, сформировать словарь в формате:

{слово_1: количество_1, слово_2: количество_2, ..., слово_N: количество_N}

То есть, ключами выступают уникальные слова (без учета регистра), а значениями - число их встречаемости в тексте.
На экран вывести значение словаря для слова (союза) 'и'. Если такого ключа нет, то вывести 0.

Sample Input:

И что сказать и что сказать и нечего и точка
Sample Output:

4
"""

# # lst_in = input().split()
# lst_in = ['И', 'что', 'сказать', 'и', 'что', 'сказать', 'и', 'нечего', 'и', 'точка']
# lst_in_low = [i.lower() for i in lst_in]
# set_l = {i for i in lst_in_low}
# count_l = [lst_in_low.count(i) for i in set_l]
#
# d = dict(zip(set_l, count_l))
#
# print(d['и']) if 'и' in set_l else print(0)



# С форума:

"""
lst = input().lower().split()
d = {i: lst.count(i) for i in set(lst)}
print(d.get('и',0))
"""


"""
lst = input().lower().split()
print({w: lst.count(w) for w in set(lst)}.get('и', 0)) 
"""

"""
Не по условиям, но коротко

print(input().lower().split().count('и'))
"""

"""
s = input().lower().split()
print({c: s.count(c) for c in s}.get('и', 0))
"""