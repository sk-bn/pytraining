"""
Подвиг 6. Вводится список книг книжного магазина в формате:

<автор 1>:<название 1>
...
<автор N>:<название N>

Авторы с названиями могут повторяться. Необходимо, используя генераторы, сформировать словарь с именем d вида:

{'автор 1': {'название 1', 'название 2', ..., 'название M'}, ..., 'автор K': {'название 1', 'название 2', ..., 'название S'}}

То есть, ключами выступают уникальные авторы, а значениями - множества с уникальными названиями книг соответствующего автора.

На экран ничего выводить не нужно, только сформировать словарь обязательно с именем d - он, далее будет проверяться в тестах!

P. S. Для считывания списка целиком в программе уже записаны начальные строчки.

Sample Input:

Пушкин: Сказака о рыбаке и рыбке
Есенин: Письмо к женщине
Тургенев: Муму
Пушкин: Евгений Онегин
Есенин: Русь
Sample Output:

True
"""

import sys

# считывание списка из входного потока
# lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список lst_in)

lst_in = ['Пушкин: Сказака о рыбаке и рыбке', 'Есенин: Письмо к женщине', 'Тургенев: Муму', 'Пушкин: Евгений Онегин',
          'Есенин: Русь']

"""
Я так и нерешил...
Списал.
Это мои черновики !!

author = [i.split(': ')[0] for i in lst_in]  # ['Пушкин', 'Есенин', 'Тургенев', 'Пушкин', 'Есенин']
novel = [i.split(': ')[1] for i in
         lst_in]  # ['Сказака о рыбаке и рыбке', 'Письмо к женщине', 'Муму', 'Евгений Онегин', 'Русь']

d = {}
for i in range(len(lst_in)):
    d[author[i]].add(novel[i])
# if author not in d:
#     d[author] = {novel}
# else:
#     d[author] |= {novel}

# d = dict(zip(key, value))
# d = {author: {author: novel for author, novel in lst_in}}

print(d)
"""
# Это я списал, разберусь завтра, голова не работает уже

# lst_in = list(map(str.strip, sys.stdin.readlines()))
d = {}
D = [(L.split(": ")[0], L.split(": ")[1]) for L in lst_in]
for i in D:
    x,y = i[0],i[1]
    if x not in d:
        d[x] = {y}
    else:
        d[x].add(y)

print(d)

"""
d = {i.split(':')[0] : {j.split(': ')[1] for j in lst_in if i.split()[0]==j.split()[0]} for i in lst_in}

# разбивка на шаги

# print({i.split(':')[0] for i in lst_in})
# print({j.split(': ')[1] for j in lst_in})
# d = {i.split(':')[0]:{j.split(': ')[1] for j in lst_in} for i in lst_in}
# print(d)

"""

"""
import sys

d = {}
for c in [c.split(': ') for c in map(str.strip, sys.stdin.readlines())]:
    d.setdefault(c[0], set()).add(c[1])

print(len(d))
"""


"""
import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))

d = {}
for _ in lst_in:
    k, v = _.split(': ')
    d.setdefault(k, set()).add(v)
    
"""

"""
import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))
lst_tuple=[x.split(': ') for x in lst_in]


dict= {k[0]:{l[1] for l in lst_tuple if l[0] == k[0]} for k in lst_tuple}

print(len(dict))

"""

"""
import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))
lst = [i.split(":") for i in lst_in]
d = {}
{d.setdefault(i[0], set()).add(i[1].strip()) for i in lst}
"""

