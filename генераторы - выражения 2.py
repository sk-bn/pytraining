"""
Подвиг 6. Вводится целое положительное число a.
Необходимо определить генератор, который бы возвращал модули чисел в диапазоне [-a; a],
a затем еще один, который бы вычислял кубы чисел (возведение в степень 3), возвращаемых первым генератором.

Вывести в одну строчку через пробел первые четыре значения.
(Полагается, что генератор выдает, как минимум четыре значения).

Sample Input:

3
Sample Output:

27 8 1 0
"""

a = 3
# a = int(input())

gen = (abs(i) for i in range(-a, a+1))

kub = (i**3 for i in list(gen))

print(*list(kub)[:4])

"""
def decor_cube(func):
    def wrapper(lim_num):
        return(x**3 for x in func(lim_num))
    return wrapper
@decor_cube
def gen_num(a):
    return (abs(x) for x in range((-1)*a, a+1))

gen_cube = gen_num(int(input()))
for x in range(4):
    print(next(gen_cube), end=' ')
"""

"""
(lambda a: print(*(abs(x) ** 3 for x in range(-a, -a + 4))))(int(input()))

"""

"""
a = int(input())
print(*[z**3 for z in (abs(x) for x in range(-a, a+1))][:4])
"""