"""
Подвиг 3. На вход программы поступают два целых числа a и b (a < b),
записанные в одну строчку через пробел.
Определите генератор, который бы выдавал модули целых чисел из диапазона [a; b].
В цикле выведите первые пять значений этого генератора.
Каждое значение с новой строки. (Гарантируется, что пять значений имеются).

Sample Input:

-3 3
Sample Output:

3
2
1
0
1
"""

a, b = map(int, input().split())

gen = (abs(i) for i in range(a,b+1))

count = 1
for i in gen:
    print(i, end='\n')
    count += 1
    if count > 5:
        break

"""
a, b = map(int, input().split())
gen = (abs(x) for x in range(a, b + 1))
for i in range(5):
    print(next(gen))
"""

"""
a, b = map(int, input().split())
print(*(abs(x) for x in range(a, a + 5)), sep = '\n')
"""

"""
a, b = map(int, input().split())
print(*(abs(x) for x in range(a, a + 5)), sep = '\n')

# * - распаковывает генератор
# - (abs(x) for x in range(a, a + 5)) сам генератор
# - sep разделитель, можно поставить любое значение
# '\n' - перенос строки
# abs функция числа по модулю

# можно проще и работать должно шустрее

a, b = map(int, input().split())
for x in range(a, a + 5)):
    print(abs(x))
"""

"""
a, b = map(int, input().split())
gen = (abs(_) for _ in range(a, b + 1))
[print(next(gen)) for _ in '+++++']
"""