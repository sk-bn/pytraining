"""
Подвиг 7. Используя символы малых букв латинского алфавита (строка ascii_lowercase):

from string import ascii_lowercase

запишите генератор, который бы возвращал все сочетания из двух букв латинского алфавита.
Выведите первые 50 сочетаний на экран в строку через пробел.

Например, первые семь начальных сочетаний имеют вид:

aa ab ac ad ae af ag
"""

from string import ascii_lowercase

print(ascii_lowercase)

# gen = [f'{i}{j}'
#        for i in ascii_lowercase
#        for j in ascii_lowercase
#        ]

gen = (i + j
       for i in ascii_lowercase
       for j in ascii_lowercase
       )
print(*gen[:50])

"""
from string import ascii_lowercase

gen = (i + j for i in ascii_lowercase for j in ascii_lowercase)
[print(next(gen), end= ' ') for _ in range(50)]
"""

"""
from string import ascii_lowercase

x = (i + j for i in ascii_lowercase for j in ascii_lowercase)
print(*(next(x) for _ in range(50)))
"""

"""
from string import ascii_lowercase

[print(i, end=' ')for j, i in enumerate(i+j for i in ascii_lowercase for j in ascii_lowercase)if j < 50]

"""
