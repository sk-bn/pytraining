"""
Подвиг 1. Вводится строка целых чисел через пробел. Напишите функцию,
которая преобразовывает эту строку в список чисел и возвращает их сумму.

Определите декоратор для этой функции, который имеет один параметр start - начальное значение суммы.
Примените декоратор со значением start=5 к функции и вызовите декорированную функцию для введенной строки s:

s = input()

Результат отобразите на экране.

Sample Input:

5 6 3 6 -4 6 -1
Sample Output:

26
"""


def plus(start=5):
    return lambda func: lambda *args: func(*args) + start


@plus()
def summa(s):
    return sum([int(x) for x in s.split()])


print(summa(input()))

# nen вместо объявления вложенной функции wrapper,
# объявляется вложенная анонимная функция (с помощью lambda)
# и тут же возвращается по команде return вместо return wrapper.

"""
from functools import wraps

def tag_decarator(tag = 'h1'):
    def dec(func):
       @wraps(func)
       def wrapper(*args):
                  return f'<{tag}>{func(*args)}</{tag}>'
       return wrapper 
    return dec

@tag_decarator(tag='div')                  
def str_lower(s):
    return s.lower()


s = input()
print(str_lower(s))
"""