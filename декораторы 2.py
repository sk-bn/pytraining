"""
Подвиг 2. На вход программы поступает строка с названиями пунктов меню,
записанные в одну строчку через пробел.
Необходимо задать функцию с именем get_menu, которая преобразует эту строку в список из слов и возвращает этот список.
Сигнатура функции, следующая:

def get_menu(s): ...

Определите декоратор для этой функции с именем show_menu, который отображает список на экран в формате:
1. Пункт_1
2. Пункт_1
...
N. Пункт_N

Примените декоратор show_menu к функции get_menu, используя оператор @.
Более ничего в программе делать не нужно. Сами функции не вызывать.

Sample Input:

Главная Добавить Удалить Выйти
Sample Output:

1. Главная
2. Добавить
3. Удалить
4. Выйти
"""

str_in = input()


# str_in = 'Главная Добавить Удалить Выйти'


def show_menu(funk):
    def wrapper(*args, **kwargs):
        result = funk(*args, **kwargs)
        # print(result)

        count = 1
        for i in result:
            print(f'{count}. {i}')
            count += 1

        return result

    return wrapper


@show_menu
def get_menu(s):
    return s.split()


# print(get_menu(str_in))
get_menu(str_in)

"""
def show_menu(func):
    def wrapper(*args, **kwards):
        for pos, dish in enumerate(func(*args, **kwards), start=1):
            print(f'{pos}. {dish}')
    return wrapper        
            
@show_menu
def get_menu(s):
    return s.split()
"""

"""
def show_menu(fn):
    def wropper(s):
        for i, value in enumerate(fn(s), start=1):
            print(f'{i}. {value}')
    return wropper

@show_menu
def get_menu(s):
    return s.split()
"""

"""
def show_menu(func):
    def cout(*args, **kwargs):
        res = func(*args, **kwargs)
        [print(f'{i+1}. {res[i]}') for i in range(len(res))]
    return cout

@show_menu
def get_menu(s):
    return s.split()
"""