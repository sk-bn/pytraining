"""
Подвиг 3. На вход программы поступает строка из целых чисел, записанных через пробел.
Напишите функцию get_list, которая преобразовывает эту строку в список из целых чисел и возвращает его.
Определите декоратор для этой функции, который сортирует список чисел по возрастанию.
Результат сортировки должен возвращаться при вызове декоратора.

Вызовите декорированную функцию get_list и отобразите полученный отсортированный список lst командой:

print(*lst)

Sample Input:

8 11 -5 4 3 10
Sample Output:

-5 3 4 8 10 11
"""

str_in = input()
# str_in = '8 11 -5 4 3 10'


def sort_funk(funk):
    def wrapper(*args, **kwargs):
        result = funk(*args, **kwargs)
        sor_result = sorted(result)
        return sor_result

    return wrapper


@sort_funk
def get_list(s: str) -> list:
    return [int(i) for i in s.split()]


lst = get_list(str_in)
print(*lst)

"""
def show_sorted(func): 
    return lambda *args, **kwards: sorted(func(*args, **kwards))  

@show_sorted
def get_list(s):
    return list(map(int, s.split()))

print(*get_list(input()))
"""

"""
def sort_numbers(func):
    def wrapper(*args, **kwargs):
        return sorted(func(*args, **kwargs))
    return wrapper


@sort_numbers
def get_list(line):
    return map(int, line.split())


lst = get_list(input())
print(*lst)
"""

"""
def sort_increase(func):
    def wrapper(*args, **kwargs):
        return sorted(func(*args, **kwargs), key=int)
    return wrapper


@sort_increase
def get_list(lst):
    return lst.split()


print(*get_list(input()))
"""