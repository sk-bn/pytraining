"""
Подвиг 4. Вводятся две строки из слов (слова записаны через пробел).
Объявите функцию, которая преобразовывает эти две строки в два списка слов и возвращает эти списки.

Определите декоратор для этой функции, который из двух списков формирует словарь,
в котором ключами являются слова из первого списка, а значениями - соответствующие элементы из второго списка.
Полученный словарь должен возвращаться при вызове декоратора.

Примените декоратор к первой функции и вызовите ее для введенных строк.
Результат (словарь d) отобразите на экране командой:

print(*sorted(d.items()))

Sample Input:

house river tree car
дом река дерево машина
Sample Output:

('car', 'машина') ('house', 'дом') ('river', 'река') ('tree', 'дерево')
"""

str_1 = input()
str_2 = input()


# str_1 = 'house river tree car'
# str_2 = 'дом река дерево машина'


def for_dict(func):
    def wrapper(*args, **kwargs):
        result_func = func(*args, **kwargs)

        result = dict(zip(result_func[0], result_func[1]))
        return result

    return wrapper


@for_dict
def modification(s1, s2):
    str_1_1 = s1.split()
    str_2_2 = s2.split()
    return str_1_1, str_2_2


d = modification(str_1, str_2)
print(*sorted(d.items()))

"""
def make_dict(func):
    def wrapper(*args, **kwargs):
        return dict(zip(*func(*args, **kwargs)))
    return wrapper

@make_dict
def make_list(*args):
    return [_.split() for _ in args]

d = make_list(input(), input())
print(*sorted(d.items()))
"""

"""
def show_sorted(func): 
    return lambda *args, **kwards: dict(zip(*func(*args, **kwards)))

@show_sorted
def get_lists(s1, s2):
    return s1.split(), s2.split()

print(*sorted(get_lists(input(), input()).items()))
"""

"""
def sort_list(fn):
    def wropper(*args):
        return {key: value for key, value in zip(*fn(*args))}
    return wropper

@sort_list
def get_list(s1, s2):
    return (s1.split(), s2.split())

s1, s2 = input(), input()
d = get_list(s1, s2)
print(*sorted(d.items()))
"""