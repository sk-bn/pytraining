"""
Подвиг 5. Объявите функцию, которая принимает строку на кириллице и преобразовывает ее в латиницу,
используя следующий словарь для замены русских букв на соответствующее латинское написание:

t = {'ё': 'yo', 'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ж': 'zh',
     'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p',
     'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh',
     'щ': 'shch', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya'}
Функция должна возвращать преобразованную строку.
Замены делать без учета регистра (исходную строку перевести в нижний регистр - малые буквы).
Все небуквенные символы ": ;.,_" превращать в символ '-' (дефиса).

Определите декоратор для этой функции, который несколько подряд идущих дефисов, превращает в один дефис.
Полученная строка должна возвращаться при вызове декоратора. (Сам декоратор на экран ничего выводить не должен).

Примените декоратор к первой функции и вызовите ее для введенной строки s на кириллице:

s = input()

Результат работы декорированной функции отобразите на экране.

Sample Input:

Python - это круто!
Sample Output:

python-eto-kruto!
"""
t = {'ё': 'yo', 'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ж': 'zh',
     'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p',
     'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh',
     'щ': 'shch', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya'}
# s = input()
# s = 'Python - это круто!'
s = "    Python -    - ЭтО : ;.,_ круто!  "



def del_defis(func):
    def wrapper(s):
        result = func(s)
        while '--' in result:
            result = result.replace('--', '-')
        # while func(s).count('--'):
        #     s = func(s).replace('--', '-')
        # return s
        return result

    return wrapper


@del_defis
def translated_str_in(s):
    s = s.strip().lower()
    new_s = []
    for i in s:
        if i in ': ;.,_':
            i = '-'
            new_s.append(i)
        elif i in t.keys():
            i = t[i]
            new_s.append(i)
        else:
            new_s.append(i)
    res = ''.join(new_s)

    return res


t = translated_str_in(s)
print(t)

"""
def remove(func):
    def wrapper(stroka):
        ls = func(stroka).replace('-', ' ').split()
        return '-'.join(ls)
    return wrapper


@remove
def convert(strings):
    lst = [t.get(i, i) if i not in [":", ";", '.', ',', ' '] else '-' for i in strings.lower()]
    return ''.join(lst)


s = input()
print(convert(s))
"""

"""
добавляем знаки препинания ': ;.,_' в словарь dict.fromkeys(': ;.,_', '-') распаковкой словарей
методы строк maketrans и translate для шифрования/дешифрования
регулярка для удаления лишних дефисов
лямда функция в качестве вложенной функции декоратора.
Верное решение #521765359
Python 3
import re

t = {'ё': 'yo', 'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ж': 'zh',
     'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p',
     'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh',
     'щ': 'shch', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya'}

def hyphenator(func):    
    return lambda *args, **kwards: re.sub(r'-+', '-', func(*args, **kwards))

@hyphenator
def transliterate(s):
    return s.lower().translate(str.maketrans({**t, **dict.fromkeys(': ;.,_', '-')}))    
    
print(transliterate(input()))
"""