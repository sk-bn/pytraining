"""
Подвиг 1. Вводится строка целых чисел через пробел. Напишите функцию,
которая преобразовывает эту строку в список чисел и возвращает их сумму.

Определите декоратор для этой функции, который имеет один параметр start - начальное значение суммы.
Примените декоратор со значением start=5 к функции и вызовите декорированную функцию для введенной строки s:

s = input()

Результат отобразите на экране.

Sample Input:

5 6 3 6 -4 6 -1
Sample Output:

26

"""
# s = input()
s = '5 6 3 6 -4 6 -1'


def dekor_with_arg(start):
    def dekor(func):
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs) + start
            return result
        return wrapper
    return dekor


@dekor_with_arg(start=5)
def mod_str_to_int(str_in: str) -> int:
    res = sum(list(map(int, s.split())))
    return res


print(mod_str_to_int(s))

"""
Источник вдохновения был тут - https://docs-python.ru/tutorial/dekoratory-python/dekoratory-argumentami/

Верное решение #566487537
Python 3
from functools import wraps

class Adding:
    # запоминаем аргументы декоратора
    def __init__(self, start=1):
        self._start = start

    # декоратор общего назначения
    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            val = func(*args, **kwargs)
            val += self._start
            return val
        return wrapper

@Adding(start=5)
def str_sum(s):
    return sum((int(_) for _ in s.split()))

s = input()
print(str_sum(s))
"""

"""
def plus(start=5):
        return lambda func: lambda *args: func(*args) + start


@plus()
def summa(s):
    return sum([int(x) for x in s.split()])

print(summa(input()))
"""