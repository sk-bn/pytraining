"""
Подвиг 2. Объявите функцию, которая возвращает переданную ей строку в нижнем регистре (с малыми буквами).
Определите декоратор для этой функции, который имеет один параметр tag,
определяющий строку с названием тега и начальным значением "h1".
Этот декоратор должен заключать возвращенную функцией строку в тег tag и возвращать результат.

Пример заключения строки "python" в тег h1: <h1>python</h1>

Примените декоратор со значением tag="div" к функции и вызовите декорированную функцию для введенной строки s:

s = input()

Результат отобразите на экране.

Sample Input:

Декораторы - это классно!
Sample Output:

<div>декораторы - это классно!</div>
"""
# s = input()
s = 'Декораторы - это классно!'


def decorator(tag='h1'):
    return lambda func: lambda *args: f'<{tag}>{func(*args)}</{tag}>'


@decorator(tag="div")
def niz_registr(s):
    return s.lower()


print(niz_registr(s))
