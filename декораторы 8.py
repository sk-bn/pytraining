"""
Подвиг 3. Объявите функцию, которая принимает строку на кириллице и преобразовывает ее в латиницу,
используя следующий словарь для замены русских букв на соответствующее латинское написание:

t = {'ё': 'yo', 'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ж': 'zh',
     'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p',
     'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh',
     'щ': 'shch', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya'}
Функция должна возвращать преобразованную строку.
Замены делать без учета регистра (исходную строку перевести в нижний регистр - малые буквы).

Определите декоратор с параметром chars и начальным значением " !?",
который данные символы преобразует в символ "-" и, кроме того,
все подряд идущие дефисы (например, "--" или "---") приводит к одному дефису.
Полученный результат должен возвращаться в виде строки.

Примените декоратор с аргументом chars="?!:;,. " к функции и вызовите декорированную функцию для введенной строки s:

s = input()

Результат отобразите на экране.

Sample Input:

Декораторы - это круто!
Sample Output:

dekoratory-eto-kruto-
"""
# s = input()
s = 'Декораторы - это круто!'

t = {'ё': 'yo', 'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ж': 'zh',
     'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p',
     'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh',
     'щ': 'shch', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya'}


def del_defis(chars=' !?'):
    def decorator(func):
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            res = []
            for i in result:
                if i not in chars:
                    res.append(i)
                else:
                    res.append('-')

            res = ''.join(res)

            while '--' in res:
                res = res.replace('--', '-')
            return res

        return wrapper

    return decorator


@del_defis()
def niz_translate(s):
    s2 = s.strip().lower()
    res = []
    for i in s2:
        if i in t.keys():
            i = t[i]
            res.append(i)
        else:
            res.append(i)
    return res


print(niz_translate(s))

"""
import re

t = {'ё': 'yo', 'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ж': 'zh',
     'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p',
     'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh',
     'щ': 'shch', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya'}

def code(s, cipher):
    return s.translate(str.maketrans(cipher))

def punctuator(chars='!?'):
    def hyphenator(func):   
        def wrapper(*args, **kwards):
            return re.sub(r'-+', '-', code(func(*args, **kwards), dict.fromkeys(chars, '-')))
        return wrapper
    return hyphenator

@punctuator(chars='?!:;,. ')
def transliterator(s):
    return code(s.lower(), t)    
    
print(transliterator(input()))
"""

"""
def set_chars(chars = '!?'):
    def decor_str(f):
        def func(a):
            b = ''
            for i in f(a):
                b += '-' if i in chars else i
                b = b.replace('--', '-')
            return b
        return func
    return decor_str
        

def get_str(s):
    return ''.join([t[i] if i in t else i for i in s.lower()])


get_str = set_chars(chars='?!:;,. ')(get_str)
print(get_str(input()))
"""

"""
def decor_text(chars=" !?"):
    def del_hyphen(func):
        def hyphen(*args):
            res = ''.join([i if i not in chars else "-" for i in func(*args)]).replace('---', '-')
            return res
        return hyphen
    return del_hyphen

@decor_text(chars="?!:;,. ") 
def get_translit(st, sep="-"):
    res = ''
    st = st.lower()
    for i in st:
            res += t.get(i, i)
    return res


s = input()
print(get_translit(s))
"""
