"""
Подвиг 3. Используя замыкания функций, объявите внутреннюю функцию,
которая заключает в тег h1 строку s (s - строка, параметр внутренней функции).
Далее, на вход программы поступает строка и ее нужно поместить в тег h1 с помощью реализованного замыкания.
Результат выведите на экран.

P. S. Пример добавления тега h1 к строке "Python": <h1>Python</h1>

Sample Input:

Balakirev
Sample Output:

<h1>Balakirev</h1>
"""
string_in = input()


def st_tag():
    def st_in(s):
        # nonlocal st_tag
        f = f'<h1>{s}</h1>'
        return f

    return st_in


st = st_tag()
print(st(string_in))

"""
# можно с разными тегами использовать

def lock_in_tag(tag):
    def add_str(string):
        return f'<{tag}>{string}</{tag}>'
    return add_str


s = input()
f = lock_in_tag('h1')
print(f(s))
"""

"""
def counter_add():
    def counter_in(s):
        return f'<h1>{s}</h1>'
    return counter_in

s = input()

print(counter_add()(s))
"""