"""
Подвиг 4. Используя замыкания функций, объявите внутреннюю функцию,
которая заключает строку s (s - строка, параметр внутренней функции) в произвольный тег,
содержащийся в переменной tag - параметре внешней функции.

Далее, на вход программы поступают две строки: первая с тегом, вторая с некоторым содержимым.
Вторую строку нужно поместить в тег из первой строки с помощью реализованного замыкания. Результат выведите на экран.

P. S. Пример добавления тега h1 к строке "Python": <h1>Python</h1>

Sample Input:

div
Сергей Балакирев
Sample Output:

<div>Сергей Балакирев</div>
"""

def lock_in_tag(tag):
    def add_str(string):
        return f'<{tag}>{string}</{tag}>'
    return add_str


tag_in = input()
s = input()
f = lock_in_tag(tag_in)
print(f(s))


"""
def counter_add(tag):
    def counter_in(s):
        return f'<{tag}>{s}</{tag}>'
    return counter_in

tag = input()
s = input()

print(counter_add(tag)(s))
"""

"""
def headerer(tag):
    def inner(string):
        return f"<{tag}>{string}</{tag}>"
    return inner

tag, text = input(), input()
print(headerer(tag)(text))
"""

"""
def wrap(tag_name: str):
    return lambda string: f'<{tag_name}>{str(string)}</{tag_name}>'

tag = wrap(input())
print(tag(input()))
"""