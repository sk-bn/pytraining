"""
Подвиг 5. Используя замыкания функций, объявите внутреннюю функцию,
которая преобразует строку из списка целых чисел, записанных через пробел, либо в список, либо в кортеж.
Тип коллекции определяется параметром tp внешней функции.
Если tp = 'list', то используется список, иначе (при другом значении) - кортеж.

Далее, на вход программы поступают две строки:
первая - это значение для параметра tp;
вторая - список целых чисел, записанных через пробел.
С помощью реализованного замыкания преобразовать эти данные в соответствующую коллекцию.
Результат вывести на экран командой (lst - ссылка на коллекцию):

print(lst)

Sample Input:

list
-5 6 8 11 0 111 -456 3
Sample Output:

[-5, 6, 8, 11, 0, 111, -456, 3]
"""
tp = input()
lst_in = list(map(int, input().split()))


def collection_in(tp):
    def modification(lst_in):
        if tp == 'list':
            return list(lst_in)
        else:
            return tuple(lst_in)

    return modification


lst = collection_in(tp)(lst_in)
print(lst)

"""
Развернутый вариант:

def parse(tp='list'):
    def inner(s):
        return (tuple, list)[tp == 'list'](map(int, s.split()))
    return inner

pr = parse(input())
print(pr(input()))

Сокращенный варинат с лямбда функцией:

def parse(tp='list'):
    return lambda s: (tuple, list)[tp == 'list'](map(int, s.split()))

pr = parse(input())
print(pr(input()))

Алекс Глозман
в прошлом году

@Андрей_Астанаев, (tuple, list)[tp == 'list'] это обращение к элементу кортежа по индексу. 
Кортеж состоит из двух типов, которые по совместительству являются ссылками на конструктор 
соотвествующего класса. Индексом в кортеже будет значение логического выражения tp == 'list' 
приведенного к целому значению. Выбранный конструктор будет применен к объекту map. 
Например, если выполняется tp == 'list'  , то значение True будет приведено к числу 1. 
Из кортежа выберется элемент по индексу 1 - это будет конструктор списка list. 
В итоге выполнится list(map(int, s.split())).
"""

"""
через eval пока никто не решил

Верное решение #492393131
Python 3
# put your python code here
def convert_string(data_type):
    def inner(numbers_string):
        return eval(f'{data_type}(map(int, numbers_string.split()))')
    return inner

print(
    convert_string(input())(input())
)

"""