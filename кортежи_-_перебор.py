"""
Подвиг 6. Вводятся имена студентов в одну строчку через пробел.
На их основе формируется кортеж. Отобразите на экране все имена из этого кортежа,
которые содержат фрагмент "ва" (без учета регистра).
Имена выводятся в одну строчку через пробел в нижнем регистре (малыми буквами).

Sample Input:

Петя Варвара Венера Василиса Василий Федор
Sample Output:

варвара василиса василий
"""

# t_in = tuple(input().split())
# # t_in = ('Петя', 'Варвара', 'Венера', 'Василиса', 'Василий', 'Федор')
#
# t = [i.lower() for i in t_in if 'ва' in i.lower()]
#
# print(*t)

"""
Можно в одну строку

print(*tuple(filter(lambda x: "ва" in x, input().lower().split())))

Или

print(*(name for name in input().lower().split() if 'ва' in name))
"""

"""
Подвиг 7. Вводятся целые числа в одну строку через пробел. На их основе формируется кортеж. 
Необходимо создать еще один кортеж с уникальными (не повторяющимися) значениями из первого кортежа. 
Результат отобразите в виде списка чисел через пробел.
P. S. Подобные задачи решаются, как правило, с помощью множеств, но в качестве практики пока обойдемся без них.

Sample Input:

8 11 -5 -2 8 11 -5
Sample Output:

8 11 -5 -2
"""

# # t_in = tuple(map(int, input().split()))
# t_in = (8, 11, -5, -2, 8, 11, -5)
#
# t = []
# for i in t_in:
#     if i not in t:
#         t.append(i)
#
# print(*t)

"""
С форума:

e = tuple(map(int, input().split()))
d = dict.fromkeys(e)
d = tuple(d.keys())
print(*d)

tup = tuple(input().split())
tup2 = ()

Или так

for i in tup:
    if i not in tup2:
        tup2 += i,

print(*tup2)

Или так

t = tuple(input().split())
print(*(v for i, v in enumerate(t) if v not in t[:i]))

Или так

t = tuple(map(int, input().split()))
new = tuple(dict.fromkeys(t))
print(*new)

Или так

print(*tuple(dict.fromkeys(tuple(map(int, input().split())))))

Или так

print(*dict.fromkeys(input().split()))

"""

"""
Подвиг 8. Вводятся целые числа в одну строку через пробел. На их основе формируется кортеж. 
Необходимо найти и вывести все индексы неуникальных (повторяющихся) значений в этом кортеже. 
Результат отобразите в виде строки чисел, записанных через пробел.

Sample Input:

5 4 -3 2 4 5 10 11
Sample Output:

0 1 4 5
"""
t_in = tuple(map(int, input().split()))
# t_in = (5, 4, -3, 2, 4, 5, 10, 11)

t = [i for i, value in enumerate(t_in) if t_in.count(value) > 1]

print(*t)
