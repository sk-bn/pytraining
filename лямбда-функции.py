"""
Подвиг 6. В программе задана функция filter_lst (см. программу ниже), которая отбирает элементы,
переданного ей итерируемого объекта и возвращает сформированный кортеж значений.

На вход программы поступает список целых чисел,
записанных в одну строчку через пробел. Вызовите функцию filter_lst для формирования:

- кортежа из всех значений входного списка (передается в параметр it);
- кортежа только из отрицательных чисел;
- кортежа только из неотрицательных чисел (то есть, включая и 0);
- кортежа из чисел в диапазоне [3; 5]

Каждый результат работы функции следует отображать с новой строки командой:

print(*lst)

где lst - список на возвращенный функцией filter_lst.
Для отбора нужных значений формальному параметру key следует передавать соответствующие определения анонимной функции.


Sample Input:

5 4 -3 4 5 -24 -6 9 0
Sample Output:

5 4 -3 4 5 -24 -6 9 0
-3 -24 -6
5 4 4 5 9 0
5 4 4 5
"""


def filter_lst(it, key=None):
    if key is None:
        return tuple(it)

    res = ()
    for x in it:
        if key(x):
            res += (x,)

    return res


# здесь продолжайте программу
nums = list(map(int, input().split()))

f1 = None
f2 = lambda x: x < 0
f3 = lambda x: x >= 0
f4 = lambda x: 3 <= x <= 5

for f in [f1, f2, f3, f4]:
    lst = filter_lst(nums, f)
    print(*lst)

"""
# здесь продолжайте программу
lst = list(map(int, input().split()))
lambda_list = [None, lambda x: x < 0, lambda x: x >= 0, lambda x: 3 <= x <= 5]
for l in lambda_list:
    print(*filter_lst(lst, l))
"""

"""
filter_lst = lambda it, key=None: tuple(it if key is None else filter(key, it))

lst = list(map(int, input().split()))
for filter_func in (None, lambda x: x < 0, lambda x: x >= 0, lambda x: 2 < x < 6):
    print(*filter_lst(lst, key=filter_func))
"""

"""
# Через словарь функций

# здесь продолжайте программу
numbers_list = list(map(int, input().split()))
lambda_dict = {
    "None": None,
    "negative": lambda x: x < 0,
    "positive": lambda x: x >= 0,
    "range 3:5": lambda x: 2 < x < 6,
    }
for d in lambda_dict.values():
    lst =  filter_lst(numbers_list, d)
    print(*lst)
"""