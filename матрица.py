import sys

# считывание списка из входного потока
s = sys.stdin.readlines()
lst_in = [list(map(int, x.strip().split())) for x in s]

# lst_in = [list(map(int, x.strip().split())) for x in sys.stdin]

# Остановить поток данных на входе: Ctrl+D

# здесь продолжайте программу (используйте список lst_in)

# Дублируем изначальный массив
lst_in2 = lst_in

# Добавляем по нулю с каждой стороны матрицы
for i in range(len(lst_in2)):
    lst_in2[i].append(0)
    lst_in2[i].insert(0, 0)

lst_in2.append([0, 0, 0, 0, 0, 0, 0])
lst_in2.insert(0, [0, 0, 0, 0, 0, 0, 0])

# Основной блок сравнения
ans = []
for i in range(len(lst_in)):
    for j in range(len(lst_in)):
        if lst_in[i][j] == 1:
            if (lst_in2[i - 1][j + 1] + lst_in2[i + 1][j - 1] +
                lst_in2[i][j - 1] + lst_in2[i][j + 1] +
                lst_in2[i + 1][j] + lst_in2[i - 1][j] +
                lst_in2[i - 1][j - 1] + lst_in2[i + 1][j + 1]) == 0:
                ans.append('Да')
            else:
                ans.append('Нет')

if 'Нет' in ans:
    print('НЕТ')
else:
    print('ДА')

"""
# Более локоничное решение

flag = True
for i in range(len(lst_in) - 1) :
    for j in range(len(lst_in) - 1) :
        if lst_in[i][j] + lst_in[i][j + 1] + lst_in[i + 1][j] + lst_in[i + 1][j + 1] > 1 :
            flag = False
            break
    if not flag:
        break
print('ДА' if flag else 'НЕТ')
"""