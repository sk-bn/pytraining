"""
Подвиг 8. Пользователь с клавиатуры вводит названия городов, пока не введет букву q.
Определить общее уникальное число городов, которые вводил пользователь.
На экран вывести это число. Из коллекций при реализации программы использовать только множества.

Sample Input:

Уфа
Москва
Тверь
Екатеринбург
Томск
Уфа
Москва
q
Sample Output:

5
"""

city_set = []
while True:
    city = input()
    if city == 'q':
        break
    else:
        city_set.append(city)

print(len(set(city_set)))

"""
Форум:

print(len(set(i for i in iter(input, 'q'))))   # генератор множества iter будет считывать пока не попадется 'q'

в данном примере, iter будет вызывать фунцию input() и сверять прочитанное значение со 
значением своего второго аргумента, который является признаком окончания итерируемой последовательности. 
Дело в том, что у функции iter есть ещё один вариант использования с двумя аргументами. 
Если задан второй аргумент sentinel, то ожидается что первым аргументом будет вызываемый (callable) объект, 
т.е. это может быть либо функция либо вызываемый класс (класс, определивший магический метод __call__). 
Функция iter будет вызывать указанный объект на каждой итерации и сверять возвращаемое значение 
со значением аргумента sentinel.
 https://docs-python.ru/tutorial/vstroennye-funktsii-interpretatora-python/funktsija-iter/
"""