"""
Подвиг 7. Вводится натуральное число, которое может быть определено простыми множителями 1, 2, 3, 5 и 7.
Необходимо разложить введенное число на указанные простые множители и проверить,
содержит ли оно множители 2, 3 и 5 (все указанные множители)?
Если это так, то вывести ДА, иначе - НЕТ.

Sample Input:

210
Sample Output:

ДА
"""

# Мда, решение из разряда "Не лёгких путей" )))

# n = int(input())
n = 210
lst = []
s = {2, 3, 5}


# Функция сбора множителей
def prime_factors(num):
    # Сначала проверяем на двойки

    while num % 2 == 0:
        lst.append(2)
        num = num / 2

    # Далее идём по последовательности до 7
    for i in range(3, 7 + 1, 2):

        while num % i == 0:
            lst.append(i)
            num = num / i
    if num > 2:
        print(num)


prime_factors(n)

print('ДА') if len(s - set(lst)) == 0 else print('НЕТ')

"""
А вот гораздо проще !!!

print('НЕТ' if int(input()) % 30 else 'ДА')

Или

print(('НЕТ', 'ДА')[int(input()) % 30 == 0])

"""

"""
Или 

number = int(input())
st_1 = set()
st_2 = {2, 3, 5}
for n in "2357":
    N = int(n)
    while number % N == 0:
        number = number / N
        st_1.add(N)
print("ДА") if st_1 >= st_2 else print("НЕТ")
"""

"""
Или

n = int(input())
m = set()
for x in range(1, int(n ** 0.5) + 1):
    if n % x == 0:
        m.add(x)

print('ДА' if {2, 3, 5}.issubset(m) else 'НЕТ')
"""
