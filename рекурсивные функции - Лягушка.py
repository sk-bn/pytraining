"""
Подвиг 7. Лягушка прыгает вперед и может скакнуть либо на одно деление, либо сразу на два.
Наша задача определить количество вариантов маршрутов, которыми лягушка может достичь риски под номером N
(натуральное число N вводится с клавиатуры).

Решать задачу следует с применением рекурсивной функции.
Назовем ее get_path. Алгоритм решения будет следующий.
Рассмотрим, например, риску под номером 4.
Очевидно, в нее лягушка может скакнуть либо с риски номер 2, либо с риски номер 3.
Значит, общее число вариантов перемещений лягушки можно определить как:

get_path(4) = get_path(3) + get_path(2)

Аналогично будет справедливо и для любой риски N:

get_path(N) = get_path(N-1) + get_path(N-2)

А начальные условия задачи, следующие:

get_path(1) = 1
get_path(2) = 2

Реализуйте такую рекурсивную функцию,
которая должна возвращать количество вариантов перемещений лягушки для риски под номером N.

Вызовите эту функцию для введенного числа N и отобразите результат на экране.

Sample Input:

7
Sample Output:

21
"""

# N = int(input())
N = 7


def get_path(N):
    if N == 1:
        return 1
    elif N == 2:
        return 2
    else:
        return get_path(N - 1) + get_path(N - 2)


print(get_path(N))


"""
N = int(input())

def get_path(n):
    return n if n in (1, 2) else get_path(n - 1) + get_path(n - 2)

print(get_path(N))
"""

"""
# Хвостовая рекурсия
def get_path(n, f1 = 1, f2 = 2):
    return get_path(n - 1, f1 = f2, f2 = f1 + f2) if n > 1 else f1

print(get_path(int(input())))
"""

"""
def get_path(n):
    if n in (1, 2):
        return n
    return get_path(n-1) + get_path(n-2)

print(get_path(int(input())))
"""

"""
def get_path(N):
    #вариант 1:
    # if N == 1:
    #     return 1
    # elif N == 2:
    #     return 2
    # else:
    #     return get_path(N-1) + get_path(N-2)
    #вариант 2:    
    # if N<3:
    #     return [[0,1][N==1],2][N==2]
    # else:
    #     return [get_path(N-2) + get_path(N-1),0][False]
    #вариант 3:
    return [[0,1][N==1],2][N==2] if N<3 else get_path(N-2) + get_path(N-1)

print(get_path(int(input())))

"""