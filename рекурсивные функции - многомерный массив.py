"""
Подвиг 6. Имеется следующий многомерный список:

d = [1, 2, [True, False], ["Москва", "Уфа", [100, 101], ['True', [-2, -1]]], 7.89]
С помощью рекурсивной функции get_line_list создать на его основе одномерный список из значений элементов списка d.
Функция должна возвращать новый созданный одномерный список.  (Только возвращать, выводить на экран ничего не нужно.)

Вызывать функцию не нужно, только объявить со следующей сигнатурой:

def get_line_list(d,a=[]): ...
где d - исходный список; a - новый формируемый.
"""

d = [1, 2, [True, False], ["Москва", "Уфа", [100, 101], ['True', [-2, -1]]], 7.89]


def get_line_list(d: list, a=[]):
    for item in d:
        if type(item) is not list:
            a.append(item)
        else:
            get_line_list(item)
    return a
    # print(a) - с принтом тут не работает


print(get_line_list(d))

"""
d = [1, 2, [True, False], ["Москва", "Уфа", [100, 101], ['True', [-2, -1]]], 7.89]

def get_line_list(d, a=[]):
    [get_line_list(f, a) if type(f) == list else a.append(f) for f in d]
    return a

"""

"""
def get_line_list(d,a=[]):
    for j in d:
        if type(j) == list:
            get_line_list(j)
        else:
            a.append(j)
    return a
"""

"""
def get_line_list(d,a=[]):
    for n in d:
        get_line_list(n,a) if type(n) == list else a.append(n)
    return a
"""
