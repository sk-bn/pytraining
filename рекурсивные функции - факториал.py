"""
Вводится целое неотрицательное число n. Необходимо с помощью рекурсивной функции fact_rec вычислить факториал числа n.
Напомню, что факториал числа, равен: n! = 1 * 2 * 3 *...* n. Функция должна возвращать вычисленное значение.
Вызывать функцию не нужно, только объявить со следующей сигнатурой:

def fact_rec(n): ...

Sample Input:

6
Sample Output:

720
"""

# ввод числа n
n = int(input())


# здесь задается функция fact_rec  (переменную n не менять!)
def fact_rec(n):
    if n <= 1:
        return 1
    else:
        return n * fact_rec(n - 1)


print(fact_rec(n))

"""
n = int(input())

def fact_rec(n):
    return n * fact_rec(n-1) if n else 1
"""

"""
# ввод числа n
n = int(input())

# здесь задается функция fact_rec  (переменную n не менять!)
def fact_rec(n):
    if n > 0:
         return n * fact_rec(n-1)
    else:
        return 1
"""

"""
# ввод числа n
n = int(input())

# здесь задается функция fact_rec  (переменную n не менять!)
def fact_rec(n: int) -> int:
    return n * fact_rec(n - 1) if n > 0 else 1
"""

"""
n = int(input())

def fact_rec(n):
    return n and n * fact_rec(n - 1) or 1
"""

"""
# ввод числа n
n = int(input())

# здесь задается функция fact_rec  (переменную n не менять!)
fact_rec = lambda x: x * fact_rec(x - 1) if x else 1
"""