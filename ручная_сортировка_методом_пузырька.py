lst = list(map(int, input('Последовательность: ').split()))
n = len(lst)

for i in range(n - 1):
    for j in range(n-1-i):
        if lst[j] == lst[j + 1]:
            continue
        elif lst[j] > lst[j + 1]:
            lst[j], lst[j + 1] = lst[j + 1], lst[j]

print(lst)
