"""
Подвиг 4. Имеется закодированная строка с помощью азбуки Морзе. Коды разделены между собой пробелом.
Необходимо ее раскодировать, используя азбуку Морзе из предыдущего занятия.
Полученное сообщение (строку) вывести на экран.

Sample Input:

.-- ... . -...- .-- . .-. -. ---
Sample Output:

все верно

morze = {'а': '.-', 'б': '-...', 'в': '.--', 'г': '--.', 'д': '-..', 'е': '.', 'ё': '.',
         'ж': '...-', 'з': '--..', 'и': '..', 'й': '.---', 'к': '-.-', 'л': '.-..', 'м': '--',
         'н': '-.', 'о': '---', 'п': '.--.', 'р': '.-.', 'с': '...', 'т': '-', 'у': '..-',
         'ф': '..-.', 'х': '....', 'ц': '-.-.', 'ч': '---.', 'ш': '----', 'щ': '--.-',
         'ъ': '--.--', 'ы': '-.--', 'ь': '-..-', 'э': '..-..', 'ю': '..--', 'я': '.-.-', ' ': '-...-'}
"""

# тут пришлось убрать букву "ё", тк в условиях задачи её нет
morze = {'а': '.-', 'б': '-...', 'в': '.--', 'г': '--.', 'д': '-..', 'е': '.',
         'ж': '...-', 'з': '--..', 'и': '..', 'й': '.---', 'к': '-.-', 'л': '.-..', 'м': '--',
         'н': '-.', 'о': '---', 'п': '.--.', 'р': '.-.', 'с': '...', 'т': '-', 'у': '..-',
         'ф': '..-.', 'х': '....', 'ц': '-.-.', 'ч': '---.', 'ш': '----', 'щ': '--.-',
         'ъ': '--.--', 'ы': '-.--', 'ь': '-..-', 'э': '..-..', 'ю': '..--', 'я': '.-.-', ' ': '-...-'}

int_n = input().split()
# int_n = ['.--', '...', '.', '-...-', '.--', '.', '.-.', '-.', '---']

# меняем значения в словаре

morze = {v: k for k, v in morze.items()}

out_n = [morze[i] for i in int_n if i in morze]

print(*out_n, sep='')

"""
или так 

back_morze = {v: k for k, v in morze.items()}

print(*(back_morze[code] for code in input().lower().split()), sep='')
"""