"""
Подвиг 6. Вводятся данные в формате:

<день рождения 1> имя_1
<день рождения 2> имя_2
...
<день рождения N> имя_N

Дни рождений и имена могут повторяться. На их основе сформировать словарь и вывести его в формате (см. пример ниже):

день рождения 1: имя1, ..., имяN1
день рождения 2: имя1, ..., имяN2
...
день рождения M: имя1, ..., имяNM

P. S. Для считывания списка целиком в программе уже записаны начальные строчки.

Sample Input:

3 Сергей
5 Николай
4 Елена
7 Владимир
5 Юлия
4 Светлана
Sample Output:

3: Сергей
5: Николай, Юлия
4: Елена, Светлана
7: Владимир

"""

import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))
# lst_in = ['3 Сергей', '5 Николай', '4 Елена', '7 Владимир', '5 Юлия', '4 Светлана']

# здесь продолжайте программу (используйте список lst_in)

names = dict()

# формируем словарь
for i in lst_in:
    key, value = i.split()
    if key in names:
        names[key] += ', '
        names[key] += value
    else:
        names[key] = value

# выводим словарь
for key, value in names.items():
    print(f'{key}: {value}')

"""
Или так 

d = {}

for i in lst_in:
    key, value = i.split()
    d[key] = d.get(key, []) + [value]

for key, value in d.items():
    print(f'{key}: ', end='')
    print(*value, sep=', ')
    
    
Или так
import sys

d = {}
for c in map(str.split, map(str.strip, sys.stdin.readlines())):
    d.setdefault(c[0] + ':', []).append(c[1])
for k in d:
    print(k, ', '.join(d[k]))
    

Или так
import sys

lst_in, d = list(map(str.split, sys.stdin.readlines())), {}
for k, v in lst_in:
    d[k] = d.get(k, []) + [v]
[print(f"{k}: {', '.join(v)}") for k, v in d.items()]
"""