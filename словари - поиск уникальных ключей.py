"""
Подвиг 5. Вводится список целых чисел в одну строчку через пробел.
С помощью словаря выделите только уникальные (не повторяющиеся) введенные значения и,
затем, сформируйте список из уникальных чисел.
Выведите его на экран в виде набора чисел, записанных через пробел.

P. S. Такая задача, обычно решается через множества, но мы их еще не проходили, поэтому воспользуемся словарем.

Sample Input:

8 11 -4 5 2 11 4 8
Sample Output:

8 11 -4 5 2 4
"""

"""
решение через множество

lst_in = set(input().split())

print(*lst_in)
"""
# lst_in = list(input().split())
lst_in = ['8', '11', '-4', '5', '2', '11', '4', '8']

a = dict.fromkeys(lst_in)
# {'8': None, '11': None, '-4': None, '5': None, '2': None, '4': None}

out = []

for i in a:
    if i not in out:
        out.append(i)
    else:
        continue

print(*out)

"""
Или так 

print(*dict.fromkeys(input().split()))

Или так 

num = input().split()
d = dict.fromkeys(num)
lst = d.keys()
print(*lst)

Или так

print(*{i: None for i in input().split()})
"""