"""
Подвиг 5. Вводятся данные в формате ключ=значение в одну строчку через пробел.
Необходимо на их основе создать словарь, затем проверить, существуют ли в нем ключи со значениями:
'house', 'True' и '5' (все ключи - строки). Если все они существуют, то вывести на экран ДА, иначе - НЕТ.

Sample Input:

вологда=город house=дом True=1 5=отлично 9=божественно
Sample Output:

ДА
"""


# lst_in = input().split()
lst_in = ['вологда=город', 'house=дом', 'True=1', '5=отлично', '9=божественно']

d = dict(c.split('=') for c in lst_in)
# {'вологда': 'город', 'house': 'дом', 'True': '1', '5': 'отлично', '9': 'божественно'}

if 'house' in d.keys() and 'True' in d.keys() and '5' in d.keys():
    print('ДА')
else:
    print('НЕТ')

"""
Можно и короче

d = dict([i.split('=') for i in input().split()])
print('ДА' if 'house' in d and 'True' in d and '5' in d else 'НЕТ')
"""

"""
Не понял конструкцию...

print(('НЕТ', 'ДА')[{'house', 'True', '5'}.issubset(input().replace('=', ' ').split()[::2])])

"""

