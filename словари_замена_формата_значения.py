"""
Подвиг 3. Вводятся данные в формате ключ=значение в одну строчку через пробел.
Значениями здесь являются целые числа (см. пример ниже).
Необходимо на их основе создать словарь d с помощью функции dict() и вывести его на экран командой:

print(*sorted(d.items()))

Sample Input:

one=1 two=2 three=3
Sample Output:

('one', 1) ('three', 3) ('two', 2)
"""

# Моё решение
d = input().split()
# d = ['one=1', 'two=2', 'three=3']

d2 = []
for i in d:
    d2.append(i.split('='))

d3 = dict(d2)

d4 = {k: int(v) for k, v in d3.items()}

print(*sorted(d4.items()))

"""
С форума:

d = dict(c.split('=') for c in input().split())
for c in d:
  d[c] = int(d[c])
print(*sorted(d.items()))
"""

"""
lst_in = input().split()

lst = [[i.split('=')[0], int(i.split('=')[1])] for i in lst_in]

d = dict(lst)

print(*sorted(d.items())) 
"""

"""
lst = [i.split('=') for i in input().split()]
d = {i: int(v) for i, v in lst}
print(*sorted(d.items()))
"""

"""
lst = [[int(i) if i.isdigit() else i for i in str.split("=")] for str in input().split()]
d = dict(lst)
print(*sorted(d.items()))
"""

"""
s = input().split()
d = {}
for x in s:
    x = x.split('=')
    d[x[0]] = int(x[1])

print(*sorted(d.items()))
"""

"""
print(*sorted({k: int(v) for pair in input().split() for k, v in [pair.split('=')]}.items()))
"""

"""
d = {i[:-2]:int(i[-1]) for i in input().split()}
print(*sorted(d.items()))
"""