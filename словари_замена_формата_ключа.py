"""
Подвиг 4. На вход программы поступают данные в виде набора строк в формате:

ключ1=значение1
ключ2=значение2
...
ключN=значениеN

Ключами здесь выступают целые числа (см. пример ниже).
Необходимо их преобразовать в словарь d (без использования функции dict())
и вывести его на экран командой:

print(*sorted(d.items()))

P. S. Для считывания списка целиком в программе уже записаны начальные строчки:
import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список lst_in)

Sample Input:

5=отлично
4=хорошо
3=удовлетворительно
Sample Output:

(3, 'удовлетворительно') (4, 'хорошо') (5, 'отлично')
"""

# import sys

# считывание списка из входного потока
# lst_in = list(map(str.strip, sys.stdin.readlines()))

lst_in = ['5=отлично', '4=хорошо', '3=удовлетворительно']
# здесь продолжайте программу (используйте список lst_in)


d = list(c.split('=') for c in lst_in)
# [['5', 'отлично'], ['4', 'хорошо'], ['3', 'удовлетворительно']]


for c in d:
    c[0] = int(c[0])
# [[5, 'отлично'], [4, 'хорошо'], [3, 'удовлетворительно']]

# d2 = dict(d)

d2 = {}

for i in d:
    d2[i[0]] = i[1]

print(*sorted(d2.items()))

"""
С форума: 

import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))
d = {}
for i in lst_in:
    key, value = i.split('=')
    d[int(key)] = value
print(*sorted(d.items()))
"""

"""
import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список lst_in)
lst = [i.split('=') for i in lst_in]
d = {int(i): v for i, v in lst}
print(*sorted(d.items()))
"""

"""
import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))
d = {int(x[0]):x[2:] for x in lst_in}
print(*sorted(d.items()))
# здесь продолжайте программу (используйте список lst_in)
"""

"""
import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список lst_in)
d = {}
for s in lst_in:
    row = s.split('=')
    d[int(row[0])] = row[1]

print(*sorted(d.items()))
"""

"""
import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))

d = {}

for i in lst_in:
    lst = i.split('=')
    d[int(lst[0])] = lst[1]
    
print(*sorted(d.items()))
"""
