n = 7
p = []

for i in range(n):
    row = [1] * (i + 1)
    for j in range(i + 1):
        if j != 0 and j != i:
            row[j] = p[i - 1][j - 1] + p[i - 1][j]
    p.append(row)

for r in p:
    print(r)


"""
А все числа в треугольнике паскаля это 11 в степени N

11^0=1
11^1=11
11^2=121
11^3=1331

И так далее
"""

"""
n = int(input())
lst = []

for i in range(1, n+1): # формирует треугольник необходимого размера, со всеми значениями - 1.
    x = [1] * i
    lst.append(x)

for j, lin in enumerate(lst): # формирует треугольник Паскаля, т.е. изменяет 1 на необходимые значения.
    for x, num in enumerate(lin):
        if x != 0 and x != len(lin)-1:
            lin[x] = lst[j-1][x-1] + lst[j-1][x]

for x, num in enumerate(lst): # выводит на экран отцентрованный треугольник Паскаля.
    print(" " * (n-x), *num)
"""