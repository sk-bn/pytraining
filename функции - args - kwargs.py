"""
Подвиг 5. Объявите функцию с именем get_data_fig для вычисления периметра произвольного N-угольника.
На вход этой функции передаются N длин сторон через аргументы. Дополнительно могут быть указаны именованные аргументы:

type - булево значение True/False
color - целое числовое значение
closed - булево значение True/False
width - целое значение

Функция должна возвращать в виде кортежа периметр многоугольника и указанные значения именованных параметров в
порядке их перечисления в тексте задания (если они были переданы).
Если какой-либо параметр отсутствует, его возвращать не нужно (пропустить).

Функцию выполнять не нужно, только определить.

# >>> get_data_fig(3, 4, 5)
# (12,)
#
# >>> get_data_fig(3, 4, 5, type=True, color=256, closed=False, width=5)
# (12, True, 256, False, 5)
#
# >>> get_data_fig(3, 4, 5, width=6, color=128)
# (12, 6, 128)
"""


def get_data_fig(*args, **kwargs):
    perimeter = (sum(args),)

    if 'type' in kwargs:
        perimeter = perimeter + (kwargs['type'],)
    if 'color' in kwargs:
        perimeter = perimeter + (kwargs['color'],)
    if 'closed' in kwargs:
        perimeter = perimeter + (kwargs['closed'],)
    if 'width' in kwargs:
        perimeter = perimeter + (kwargs['width'],)

    return perimeter
    # print(perimeter)


# get_data_fig(3, 4, 5)
get_data_fig(3, 4, 5, type=True, color=256, closed=False, width=5)
get_data_fig(3, 4, 5)
get_data_fig(3, 4, 5, width=6, color=128)

"""
def get_data_fig(*args, **kwargs):
    kwargs = [kwargs[i] for i in ['type', 'color', 'closed', 'width'] if i in kwargs] 
    return (sum(args), *kwargs)
"""

"""
def get_data_fig(*n, type=None, color=None, closed=None, width=None):
    res = (sum(n),) + tuple(key for key in [type, color, closed, width] if key != None)
    return res
"""


"""
def get_data_fig(*args, **kwargs):
    return (sum(args),) + tuple(kwargs[i] for i in ['type', 'color', 'closed', 'width'] if i in kwargs)
"""


"""
def get_data_fig(*args, **kwargs):
    res = (sum(args),)
    for x in ['type', 'color', 'closed', 'width']:
        if x in kwargs:
            res += (kwargs[x],)
    return res
"""