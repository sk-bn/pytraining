"""
Подвиг 3. Вводится натуральное число N (N > 8).
Необходимо определить функцию-генератор, которая бы выдавала пароль длиной N
символов из случайных букв, цифр и некоторых других знаков.
Для получения последовательности допустимых символов для
генерации паролей в программе импортированы две строки:
ascii_lowercase, ascii_uppercase (см. листинг ниже), на основе которых формируется общий список:

from string import ascii_lowercase, ascii_uppercase
chars = ascii_lowercase + ascii_uppercase + "0123456789!?@#$*"
Функция-генератор должна при каждом вызове возвращать новый пароль
из случайно выбранных символов chars длиной N и делать это бесконечно, то есть,
вызывать ее можно бесконечное число раз.
Сгенерировать случайный индекс indx в диапазоне [a; b]
для символа можно с помощью функции randint модуля random:

import random
random.seed(1)
indx = random.randint(a, b)
Сгенерируйте с помощью этой функции первые пять паролей и
выведите их в столбик (каждый с новой строки).

Sample Input:

10
Sample Output:

riGp?58WAm
!dX3a5IDnO
dcdbWB2d*C
4?DSDC6Lc1
mxLpQ@2@yM

"""
import random
from string import ascii_lowercase, ascii_uppercase

# установка зерна датчика случайных чисел (не менять)
random.seed(1)


# здесь продолжайте программу

def pasw(N):
    chars = ascii_lowercase + ascii_uppercase + "0123456789!?@#$*"
    strg = ""

    for j in range(5):
        for i in range(N):
            strg = strg + chars[random.randint(0, len(chars))]
        yield strg
        strg = ""


# N = int(input())
N = 10
for i in pasw(N):
    print(i)

"""
import random
from string import ascii_lowercase, ascii_uppercase
chars = ascii_lowercase + ascii_uppercase + "0123456789!?@#$*"

# установка зерна датчика случайных чисел (не менять)
random.seed(1)

n = int(input())

def get_pass(n):
    while True:
        passw = ''
        for i in range(n):
            indx = random.randint(0, len(chars)-1)
            passw += chars[indx]
        yield passw

gen = get_pass(n)        
        
for i in range(5):
    print(next(gen))
"""

"""
import random
from string import ascii_lowercase, ascii_uppercase

# установка зерна датчика случайных чисел (не менять)
random.seed(1)

# здесь продолжайте программу
def gen_pwd(n):
    chars = ascii_lowercase + ascii_uppercase + "0123456789!?@#$*"
    while True:
        yield ''.join([chars[random.randint(0, len(chars))] for _ in range(n)])

pwd = gen_pwd(int(input()))
[print(next(pwd)) for _ in range(5)]
"""

"""
import random
from string import ascii_lowercase, ascii_uppercase

# установка зерна датчика случайных чисел (не менять)
random.seed(1)
chars = ascii_lowercase + ascii_uppercase + "0123456789!?@#$*"

def password(n):
    for i in range(n):
        indx = random.randint(0, len(chars) - 1)
        yield chars[indx]

x = int(input())
for _ in range(5):
    print(*password(x), sep='')
"""

"""
import random
from string import ascii_lowercase, ascii_uppercase

# установка зерна датчика случайных чисел (не менять)
random.seed(1)
chars = ascii_lowercase + ascii_uppercase + "0123456789!?@#$*"

def gen_pass(n):
    password = ''
    for _ in range(n):
        indx = random.randint(0, len(chars))
        password += chars[indx]
    yield password

N = int(input())
for _ in range(5):
    print(*gen_pass(N))
"""
