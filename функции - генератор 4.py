"""
Подвиг 4. Вводится натуральное число N. Используя строки из латинских букв ascii_lowercase и ascii_uppercase:

from string import ascii_lowercase, ascii_uppercase
chars = ascii_lowercase + ascii_uppercase
задайте функцию-генератор, которая бы возвращала случайно
сформированные email-адреса с доменом mail.ru и длиной в N символов.
Например, при N=6, получим адрес: SCrUZo@mail.ru

Для формирования случайного индекса для строки chars используйте функцию randint модуля random:

import random
random.seed(1)
indx = random.randint(0, len(chars)-1)
Функция-генератор должна возвращать бесконечное число таких адресов, то есть, генерировать постоянно.
Выведите первые пять сгенерированных email и выведите их в столбик (каждый с новой строки).

Sample Input:

8
Sample Output:

iKZWeqhF@mail.ru
WCEPyYng@mail.ru
FbyBMWXa@mail.ru
SCrUZoLg@mail.ru
ubbbPIay@mail.ru
"""

import random
from string import ascii_lowercase, ascii_uppercase

random.seed(1)

def e_mail(N):
    chars = ascii_lowercase + ascii_uppercase
    strg = ""

    for j in range(5):
        for i in range(N):
            # indx = random.randint(0, len(chars) - 1)
            strg = strg + chars[random.randint(0, len(chars))]
        yield strg + "@mail.ru"
        strg = ""

# N = int(input())
N = 8

for i in e_mail(N):
    print(i)

"""
import random
from string import ascii_lowercase, ascii_uppercase
chars = ascii_lowercase + ascii_uppercase

# установка зерна датчика случайных чисел (не менять)
random.seed(1)

n = int(input())

def get_mail(n):
    while True:
        mail = ''
        for i in range(n):
            indx = random.randint(0, len(chars)-1)
            mail += chars[indx]
        yield mail + '@mail.ru'

gen = get_mail(n)        
        
for i in range(5):
    print(next(gen))
"""

"""
import random
from string import ascii_lowercase, ascii_uppercase
chars = ascii_lowercase + ascii_uppercase
random.seed(1)


def add_domen(e="@wrap"):
    def wrapper(mail):
        return f"{mail + e}"
    return wrapper


def get_char(n):
    for i in range(n):
        char = chars[random.randint(0, len(chars) - 1)]
        yield char


ln = int(input())
domen = "@mail.ru"
pswrd = add_domen(domen)
for n in range(5):
    print(pswrd("".join(get_char(ln))))
"""

"""
import random
from string import ascii_lowercase, ascii_uppercase

def gen_email(n):
    while True:
        yield ''.join(chars[random.randrange(len(chars))] for _ in range(n)) + '@mail.ru'
        
random.seed(1)
chars = ascii_lowercase + ascii_uppercase
egen = gen_email(int(input()))
for _ in range(5):
    print(next(egen))
"""

"""
import random
from string import ascii_lowercase, ascii_uppercase

random.seed(1)

def super_emeil(x: 'int', y: 'str'):
	yield [random.choice(y) for __ in range(x)]

x = int(input())
chars = ascii_lowercase + ascii_uppercase
[print(''.join(*super_emeil(x, chars)), end='@mail.ru\n') for __ in range(5)]
"""