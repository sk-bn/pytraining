"""
Подвиг 5. Определите функцию-генератор, которая бы возвращала простые числа.
(Простое число - это натуральное число, которое делится только на себя и на 1).
Выведите с помощью этой функции первые 20 простых чисел (начиная с 2)
в одну строчку через пробел.
"""
def get_sample_numbers(n=2):
    while True:
        if all((n % i) for i in range(2, int(n ** 0.5) + 1)):
            yield n
        n += 1


gen = get_sample_numbers()
print(*(next(gen) for _ in range(20)))

"""
n ** 0.5 это квадратный корень, предельное число, после которого можно прерывать цикл
range(2, limit) выдает последовательность
n % i - остатки от деления нацело
all - даст True только если интерпретирует все элементы как True, 
в данном случае, все ненулевые, то есть, не делящиеся нацело

Если хоть одно число делится, результат будет False
"""

"""
def prime_gen():
    for i in range(2, 10 ** 10):
        for j in range(2, int(i ** 0.5) + 1):
            if i % j == 0:
                break
        else:
            yield i


p = prime_gen()
print(*(next(p) for i in range(20)))
"""

"""
import sympy
def gen_simple():
    i = 1
    while True:
        yield sympy.prime(i)
        i += 1
g = gen_simple()        
print(*(next(g) for _ in range(20)))
"""

"""
def is_prime(n):
    d = 2
    while d * d <= n and n % d != 0:
        d += 1
    return d * d > n

def get_prime():
    n = 2
    while True:
        if is_prime(n):
            yield n
        n += 1

gen = get_prime()

print(*(next(gen) for i in range(20)))
"""