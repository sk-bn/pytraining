"""
Большой подвиг 1.
Повторите быстрый алгоритм Евклида для нахождения наибольшего общего делителя двух натуральных чисел a и b.
В программе необходимо объявить функцию get_nod, которая принимает два аргумента a и b (натуральные числа)
и возвращает вычисленное значение НОД(a, b).

P. S. Вызывать функцию не нужно, только задать. Постарайтесь реализовать алгоритм, не возвращаясь к материалу на видео.

Sample Input:

15 121050
Sample Output:

15
"""
import time


def get_nod(a: int, b: int) -> int:
    """
    Вычисляется НОД - наибольший общий делитель для натуральных чисел a и b по алгоритму Евклида.
    :param a: первое число
    :param b: второе число
    :return: НОД
    """
    while a != b:
        if a > b:
            a -= b
        else:
            b -= a
    return a


def get_nod_fast(a: int, b: int) -> int:
    """
    Быстрый алгоритм Евклида - Вычисляется НОД - наибольший общий делитель.
    :param a: первое число
    :param b: второе число
    :return: НОД
    """
    if a < b:
        a, b = b, a
    while b != 0:
        a, b = b, a % b

    return a


def test_nod(func):
    # --- тест №1 ---
    a = 28
    b = 35
    res = func(a, b)
    if res == 7:
        print('#test1 - ok')
    else:
        print('#test1 - fail')

    # --- тест №2 ---
    a = 100
    b = 1
    res = func(a, b)
    if res == 1:
        print('#test2 - ok')
    else:
        print('#test2 - fail')

    # --- тест №3 ---
    a = 2
    b = 100000000
    st = time.time()  # вычисляем время начала работы функции
    res = func(a, b)
    et = time.time()  # вычисляем время окончания работы функции
    dt = et - st  # вычисляем скорость работы функции
    if res == 2 and dt < 1:
        print('#test3 - ok')
    else:
        print('#test3 - fail')


# res = get_nod(18, 24)
res = get_nod_fast(15, 121050)

print(res)
# print(help(get_nod))
# test_nod(get_nod_fast)
