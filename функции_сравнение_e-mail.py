"""
Подвиг 8. Напишите функцию, которая проверяет корректность переданного ей email-адреса в виде строки.
Будем полагать, что адрес верен, если он обязательно содержит символы '@' и '.',
а все остальные символы могут принимать значения: 'a-z', 'A-Z', '0-9' и '_'.
Если email верен, то функция выводит ДА, иначе - НЕТ.

После объявления функции прочитайте (с помощью функции input) строку с email-адресом и вызовите функцию с этим аргументом.

Sample Input:

sc_lib@list.ru
Sample Output:

ДА
"""
t = {'A', 'E', 'I', 'O', 'U', 'Y', 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S',
     'T', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_', 'a', 'e', 'i', 'o',
     'u', 'y', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't',
     'v', 'w', 'x', 'y', 'z', '@', '.'}

def func(e_adress):
    ans = []
    for i in e_adress:
        if i in t:
            ans.append('ДА')
        else:
            ans.append('НЕТ')

    if 'НЕТ' in ans:
        print('НЕТ')
    else:
        print('ДА')

e_adress = input()
# e_adress = 'sc_lib@list.ru'
func(e_adress)

"""
Или так

def check_mail(mail):
    allow = set("abcdefghijklmnopqrstuvwxyz0123456789_@.")
    nesessary = {"@", "."}
    print("ДА") if nesessary <= mail <= allow else print("НЕТ")


msg = set(input().lower())
check_mail(msg)
"""

"""
t = {'A', 'E', 'I', 'O', 'U', 'Y', 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 
'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_', 'a', 
'e', 'i', 'o', 'u', 'y', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 
't', 'v', 'w', 'x', 'y', 'z', '@', "."}

def foo(email):
    print("ДА" if '@' in email and '.' in email and set(email) <= t else "НЕТ" )
"""