"""
Подвиг 1. Вводятся названия городов в одну строчку через пробел. Необходимо определить функцию filter,
которая бы возвращала только названия длиной более 5 символов.
Извлеките первые три полученных значения с помощью функции next и отобразите их на экране в одну строчку через пробел.
(Полагается, что минимум три значения имеются).

Sample Input:

Тула Ульяновск Хабаровск Владивосток Омск Уфа
Sample Output:

Ульяновск Хабаровск Владивосток
"""

sity_in = list(input().split())
sity_out = filter(lambda x: len(x)>5, sity_in)

for i in range(3):
    print(next(sity_out), end=" ")

"""
cities = filter(lambda x: len(x) > 5, input().split())
print(*(next(cities) for _ in range(3)))
"""

"""
gen = (filter(lambda x: len(x) > 5, input().split()))
print(*(next(gen) for _ in '...'))

"""

"""
print(*list(filter(lambda x: len(x) > 5, input().split()))[: 3])
"""