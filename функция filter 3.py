"""
Подвиг 3. Вводится список целых чисел в одну строчку через пробел.
Необходимо оставить в нем только двузначные числа. Реализовать программу с использованием функции filter.
Результат отобразить на экране в виде последовательности оставшихся чисел в одну строчку через пробел.

Sample Input:

8 11 0 -23 140 1
Sample Output:

11 -23
"""

lst_in = list(map(int, input().split()))
lst_out = list(filter((lambda x: 9<x<100 or -100<x<-9), lst_in))
print(*lst_out)

"""
print(*filter(lambda x: 9 < abs(x) < 100, map(int, input().split())))
"""

"""
print(*filter(lambda x: len(str(abs(int(x)))) == 2, input().split()))
"""

"""
lst = input().split()
lst_out = filter(lambda x: 9 < abs(int(x)) < 100, lst)
print(*lst_out)
"""