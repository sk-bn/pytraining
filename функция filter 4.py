"""
Подвиг 4. Саша и Галя коллекционируют монетки.
Каждый из них решил записать номиналы монеток из своей коллекции. Получилось два списка.
Эти списки поступают на вход программы в виде двух строк из целых чисел, записанных через пробел.
Необходимо выделить значения, присутствующие в обоих списках и оставить среди них только четные.
Результат вывести на экран в виде строки полученных чисел в порядке их возрастания через пробел.

При реализации программы используйте функцию filter и кое-что еще (для упрощения программы), подумайте что.

Sample Input:

1 5 2 7 10 25 50 100
5 2 3 7 10 25 55
Sample Output:

2 10
"""
from collections import Counter

# sasha = list(map(int, input().split()))
sasha = [1, 5, 2, 7, 10, 25, 50, 100]
# galya = list(map(int, input().split()))
galya = [5, 2, 3, 7, 10, 25, 55]

common_items = list((Counter(sasha) & Counter(galya)).elements())
lst_out = filter((lambda x: x%2 == 0), common_items)

print(*lst_out)

"""
a = list(map(int, input().split()))
b = list(map(int, input().split()))

sm = sorted(filter(lambda x: x in b and x % 2 == 0, a))
print(*sm)
"""

"""
a, b = map(str.split, (input(), input()))
print(*filter(lambda a: int(a) % 2 == 0, set(a)&set(b)))
"""

"""
print(*sorted(filter(lambda x: x % 2 == 0, map(int, set(input().split()) & set(input().split())))))
"""

"""
s1 = set(map(int, input().split()))
s2 = set(map(int, input().split()))

fil = filter(lambda x: not x % 2, s1 & s2) 

print(*fil)
"""

"""
s1 = map(int, input().split())
s2 = map(int, input().split())
s = set(s1) & set(s2)
print(*sorted(filter(lambda x: x % 2 == 0, s)))
"""