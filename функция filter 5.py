"""
Подвиг 5. Вводится список email-адресов в одну строчку через пробел.
Среди них нужно оставить только корректно записанные адреса.
Будем полагать, что к таким относятся те, что используют латинские буквы, цифры и символ подчеркивания.
А также в адресе должен быть символ "@", а после него символ точки "." (между ними, конечно же, могут быть и другие символы).

Результат отобразить в виде строки email-адресов, записанных через пробел.

Sample Input:

abc@it.ru dfd3.ru@mail biba123@list.ru sc_lib@list.ru $fg9@fd.com
Sample Output:

abc@it.ru biba123@list.ru sc_lib@list.ru
"""
import string

# st_in = list(input().split())
st_in = ['abc@it.ru', 'dfd3.ru@mail', 'biba123@list.ru', 'sc_lib@list.ru', '$fg9@fd.com']


def correct_email(st: str) -> str:
    """Функция проверяет корректность e-mail адреса"""
    ok_symbols = string.ascii_letters + string.digits + '_@.'
    for i in st:
        if i not in ok_symbols:
            return False

        elif '@' in i:
            idx_sob = st.index('@')
            if '.' in st[idx_sob + 1:]:
                return st


st_out = list(filter(correct_email, st_in))
print(*st_out)
"""
from string import ascii_letters, digits
chars = ascii_letters + digits + "_@."

lst = input().split()

fil = filter(lambda adress: 
             '@' in adress and
             '.' in adress and 
             all(i in chars for i in adress) and
             adress.find('@') < adress.find('.'), lst)

print(*fil)
"""

"""
# Проверяем Email
def is_email(email):
    name, domain = email.split("@") if "@" in email else "!", "!"
    parts = name.split(".") + domain.split(".") if "." in domain else ["!"]
    return all(map(str.isidentifier, parts))
"""

"""
def is_valid_email(email):
    email = email.lower()
    if email.find("@") >= email.find("."):
        return False
    for i in email:
        if i not in "abcdefghijklmnopqrstuvwxyz1234567890_@.":
            return False
    return True
print(*[*filter(is_valid_email, input().split())])
"""

"""
print(*filter(lambda s: len(set(s.lower())-set('@._0123456789abcdefghijklmnopqrstuvwxyz'))==0 and '.' in s.split('@')[1], input().split()))
"""

"""
def foo(s: str):
    if "@" in s:
        s1, s2 = s.split("@")
        if s1.replace('_', '').isalnum() and s2.count('.'):
            return True
    return False


print(*filter(foo, input().split()))
"""

"""
chars=set('qwertyuiopasdfghjklzxcvbnmQWERTYUIPOLKJHGFDSAZXCVBNM.@1234567890_')
print(*filter(lambda x: set(x).issubset(chars) and x.find('@')<x.find('.'),input().split()))
"""