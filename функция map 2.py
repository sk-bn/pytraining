"""
Подвиг 2. На вход поступает строка из целых чисел, записанных через пробел.
С помощью функции map преобразовать эту строку в список целых чисел, взятых по модулю.
Сформируйте именно список lst из таких чисел. Отобразите его на экране в виде набора чисел, идущих через пробел.

Sample Input:

-5 6 8 11 -10 0
Sample Output:

5 6 8 11 10 0
"""

print(*[abs(i) for i in map(int, input().split())])

"""
lst=map(lambda x: abs(int(x)),input().split())
"""

"""
nums = map(int, input().split())
lst = [abs(i) for i in nums]
print(*lst)
"""

"""
lst = list(map(lambda x: abs(int(x)), input().split()))

print(*lst)
"""

"""
print(*list(map(abs, map(int, input().split()))))
"""