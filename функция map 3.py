"""
Подвиг 3. Вводится таблица целых чисел. Используя функцию map и генератор списков,
преобразуйте список строк lst_in (см. листинг) в двумерный список с именем lst2D, содержащий целые числа.

Выводить на экран ничего не нужно, только сформировать список lst2D на основе введенных данных.

Sample Input:

8 11 -5
3 4 10
-1 -2 3
4 5 6
Sample Output:

True
"""

import sys

# считывание списка из входного потока
# lst_in = list(map(str.strip, sys.stdin.readlines()))
lst_in = ['8 11 -5', '3 4 10', '-1 -2 3', '4 5 6']

lst2D = [list(map(int, i.split())) for i in lst_in]
# здесь продолжайте программу (используйте список lst_in)
# переменную lst_in не менять!

print(lst2D)

"""
import sys

lst_in = list(map(str.strip, sys.stdin.readlines()))
lst2D = [*map(lambda x: [*map(int, x.split())], lst_in)]
"""

"""
4 варианта:

две map()
два генератора
генератор в map()
map()  в генераторе
Верное решение #724374643
Python 3
import sys

# считывание списка из входного потока
lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список lst_in)
# переменную lst_in не менять!

# lst2D = [*map(lambda x: [*map(int, x.split())], lst_in)]
# lst2D = [[int(i) for i in x.split()] for x in lst_in]
# lst2D = [*map(lambda x: [int(i) for i in x.split()], lst_in)]
lst2D = [[*map(int, x.split())] for x in lst_in]
"""