"""
Подвиг 1. Вводятся два списка целых чисел.
Необходимо попарно перебрать их элементы и перемножить между собой.
При реализации программы используйте функции zip и map.
Выведите на экран первые три значения, используя функцию next.
Значения выводятся в строчку через пробел.
(Полагается, что три выходных значения всегда будут присутствовать).

Sample Input:

-7 8 11 -1 3
1 2 3 4 5 6 7 8 9 10
Sample Output:

-7 16 33

"""
# lst1 = list(map(int, input().split()))
# lst2 = list(map(int, input().split()))

lst1 = [-7, 8, 11, -1, 3]
lst2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

lst = list(zip(lst1, lst2))
# lst = [(-7, 1), (8, 2), (11, 3), (-1, 4), (3, 5)]
lst3 = list(map((lambda x: x[0] * x[1]), lst))
print(*lst3[:3])

"""
n=map(int, input().split())
m=map(int, input().split())
res=(x*y for x,y in zip(n, m))
for _ in range(3):
    print(next(res), end= " ")
"""

"""
a, b = [map(int, input().split()) for _ in '12']

for _ in '123':
    v1, v2 = next(zip(a, b))
    print(v1 * v2, end=' ')
"""

"""
gen = map(lambda a, b: int(a)*int(b), input().split(), input().split())
print(*(next(gen) for _ in '123'))
"""

"""
def mult(a):
    return a[0]*a[1]


l1 = list(map(int, input().split()))
l2 = list(map(int, input().split()))
res = map(mult, zip(l1,l2))
for _ in range(3):
    print(next(res), end=' ')
"""