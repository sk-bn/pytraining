"""
Подвиг 2. Вводится неравномерная таблица целых чисел.
С помощью функции zip выровнить эту таблицу, приведя ее к прямоугольному виду, отбросив выходящие элементы.
Вывести результат на экран в виде такой же таблицы чисел.

P. S. Для считывания списка целиком в программе уже записаны начальные строчки.

Sample Input:

1 2 3 4 5 6
3 4 5 6
7 8 9
9 7 5 3 2
Sample Output:

1 2 3
3 4 5
7 8 9
9 7 5
"""
import sys

# считывание списка из входного потока
# lst_in = list(map(str.strip, sys.stdin.readlines()))

# здесь продолжайте программу (используйте список строк lst_in)
lst_in = ['1 2 3 4 5 6', '3 4 5 6', '7 8 9', '9 7 5 3 2']
lst = list(list(map(int, i.split())) for i in lst_in)
a, b, c, d = lst[0], lst[1], lst[2], lst[3]
z = zip(a, b, c, d)
# t1, t2, t3, t4 = zip(*list(z))
# print(t1, t2, t3, t4, sep='\n')

for i in zip(*list(z)):
    print(*list(i), sep=' ')
# автотест не проходит...

"""
# вот правильное решение

for i in zip(*zip(*lst_in)):
    print(*i, sep='')
"""

"""
list(map(print, *zip(*map(str.split, lst_in))))

"""

"""
list(map(print, *zip(*map(lambda row: [int(x) for x in row.split()], lst_in))))
"""

"""
lst_in = list(map(str.strip, sys.stdin.readlines()))

for i in zip(*zip(*lst_in)):
    print(*i, sep='')
"""

"""
z = zip(*zip(*lst_in))
for i in z:
    print(''.join(list(i)))
"""

"""
lst_in = [list(map(int, x.split())) for x in lst_in]
# скармливаем zip нашу распакованную матрицу
res = [list(row) for row in zip(*lst_in)]
# из-за того, что в процессе матрицу повернуло, траснпонируем ее еще раз
for row in zip(*res):
    print(*row)
"""

"""

"""