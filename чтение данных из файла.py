# https://stepik.org/lesson/567068/step/1?unit=561342

file = open("my_fils.txt", encoding='utf-8')

print(file.read(4))
file.seek(0)
print(file.read(4))
pos = file.tell()
print(pos)

file.close()
 ***

file = open("my_fils.txt", encoding='utf-8')

print(file.readline(), end="")  # \n
print(file.readline(), end="")

file.close()
***

file = open("my_fils.txt", encoding='utf-8')

for line in file:
    print(line, end="")

file.close()
***

file = open("my_fils.txt", encoding='utf-8')

s = file.readlines()
print(s)

file.close()

'''написал функцию составления словаря из данных файла.  
Каждая строчка это пара ключ - значение которые разделены между собой запятой. 
Сама функция возвращает кортеж, кортеж в последствии преобразуется в словарь и выводится на экран.

Узнал что словари жрут много оперативы и если проект большой, 
то для экономии опертивной памяти целесообразно подключать файлы для хранения некоторых данных. 
Исходя из тех знаний которые получил на курсе назрело пока такое:
'''
def file_lst_splt(path,coding="utf-8"):
    """Возвращаем пары в кортеже для создания словаря"""
    lst = []
    file = open(path, encoding=coding)
    for line in file.readlines():
        if "\n" in line:
            line = line.replace("\n","")
        lst.append([x for x in line.split(",")])

    file.close()
    return lst


pth = r"data/file.txt"
lst = file_lst_splt(pth)
dct = dict(lst)

print(dct)

'''
например из файла с таким содержанием

Word, Слово
Hello, Здравствуйте
Tree, Дерево
Paper, Бумага

получаем
такой
словарь:

{'Word': ' Слово', 'Hello': ' Здравствуйте', 'Tree': ' Дерево', 'Paper': ' Бумага'}
'''

# Всё-таки решил потратить время на запись функций))

def file_to_lst(path, coding="utf-8"):
    """Считываем все строчки файла и возвращаем их в виде списка"""
    lst = []
    file = open(f"{path}", encoding=f"{coding}")
    for line in file.readlines():
        lst.append(line)
    file.close()
    return lst


def replace_strings(lst):
    """Удаляем переносы строк из списка и возвращаем список"""
    for i in range(len(lst)):
        if "\n" in lst[i]:
            lst[i] = lst[i].replace("\n", " ")

    return lst


def file_rewrite(path, lst, coding="utf-8"):
    """Записываем список в фаил"""
    file = open(f"{path}", "w", encoding=f"{coding}")
    for x in lst:
        file.write(x)
    file.close()


pth = r"data/file.txt"

lst = file_to_lst(pth)  # Извлекаем все строчки из файла в список
print(lst)  # Проверяем что мы извлекли
print()

lst = replace_strings(lst)  # Удаляем переносы из списка
print(lst)  # Проверяем результат работы функции replace_strings
print()

file_rewrite(pth, lst)  # Перезаписываем фаил без переносов
print(lst)  # Проверяем что мы перезаписали



# а можно вернуть сразу строку которую сразу же можно и установить в качестве списка на запись без прокладки в виде функции которая преобразует фаил через список


def file_to_string(path, coding="utf-8"):
    """Возвращаем все строки из файла в виде строки, удаляя переносы"""
    str = ""
    file = open(path, encoding=coding)
    for line in file.readlines():
        for simb in line:
            if simb == "\n":
                str += " "
            else:
                str += simb + ""
    file.close()
    return str


def file_to_string(path, coding="utf-8"):
    """Возвращаем все строки из файла в виде строки, удаляя переносы"""
    str = ""
    file = open(path, encoding=coding)
    for line in file.readlines():
        for simb in line:
            if simb == "\n":
                str += " "
            else:
                str += simb
    file.close()
    return str


def file_write(path, str, coding="utf-8"):
    """Записываем строку в фаил"""
    file = open(path, "w", encoding=coding)
    file.write(str)
    file.close()


pth = r"data/file.txt"

file_write(pth, file_to_string(pth))


# а можно вообще все сделать одной функцией

def file_del_str(path,coding="utf-8"):
    """Удаляем переносы строк из файла"""
    str = ""
    file = open(path, encoding=coding)
    for line in file.readlines():
        for simb in line:
            if simb =="\n":
                str+=" "
            else:
                str+=simb
    file.close()

    file = open(path,"w", encoding=coding)
    file.write(str)
    print("done!")



pth = r"data/file.txt"

file_del_str(pth)



"""
что первое пришло в голову как можно поиграться с файлами(из-за отстутсвия заданий).

В проекте создаем папку data  в нем создаем файл file2.txt  и заполняем какими то строками к примеру:

Привет как дела?
ну здравствуй, как ты?
да так ниче пойдет
ну ок я пошел
ну ок давай
 

Потом в файле  проект пишем такой скрипт, который удаляет знаки \n  в конце строчек и перезаписывает 
фаил с удаленными переносами строки. 
Было бы   больше времени еще бы поигрался с функциями чтобы не писать километры каждый раз 
"""

file = open(r"data/file2.txt", encoding="utf-8")
lst = file.readlines()
file.close()

print(lst)

for i in range(len(lst)):
    if "\n" in lst[i]:
        lst[i] = lst[i].replace("\n", "")

print(lst)

file = open(r"data/file2.txt", "w", encoding="utf-8")
for x in lst:
    file.write(x)
file.close()

file = open(r'data/file2.txt', encoding="utf-8")
for string in file.readlines():
    print(string)
file.close